import os, sys
import csv
import threading, queue, time
import datetime
from broadway2 import Broadway, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes


def main(args):
    dt = datetime.datetime.now()

    if len(args) > 1:
        csvFile = args[1]
    else:
        csvFile = os.path.join(os.path.split(args[0])[0], 'validate.csv')

    logFile = os.path.splitext(csvFile)[0] + '_' + datetime.datetime.strftime(dt, '%Y-%m-%dT%H-%M-%S') + '.log'

    multiRawTx = MultiRawSender()


    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    # get singleton to get/access all connected adapters
    base = Broadway()

    if base.Count < 1:
        print('No Adapters found!')
        sys.exit(-1)

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
        devTx = base.open_raw_tx(a.Inst)

        multiRawTx.add(a, devTx)

        devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType1)

        multiRawRx.add(a, devRx)

    try:
        fCsv = open(csvFile, 'r', encoding='UTF-8')
    except:
        return -1

    try:
        fLog = open(logFile, 'w', encoding='UTF-8')
    except:
        return -2

    # start Thread and enable all raw-receiver devices
    multiRawRx.rx_enable()
    multiRawRx.start()
    time.sleep(0.1)

    reader = csv.reader(fCsv, delimiter=',')
    head = next(reader)
    if head == ['Stage', 'Name', 'Dir', 'TgtAddr', 'SrcAddr', 'EthType', 'Data']:
        errCnt = 0
        lastStage = ''
        stage = ''
        txFrames = []
        rxFramesExpected = []
        rxFramesOptional = []
        stopRequest = False
        for row in reader:
            if len(row) == 7:
                stage = row[0]
            if stage != lastStage: # new stage found
                if lastStage != '': # send Out
                    print('Stage ready: {0} {1} / {1}'.format(lastStage, len(txFrames), len(rxFramesExpected)))
                    lines = []
                    for txRow in txFrames:
                        dev = txRow[1]
                        print('send out: {0}'.format(dev))
                        frames = [(txRow[3], txRow[5], txRow[6])]
                        res = multiRawTx.send_raw_frames(dev, txRow[4], frames)
                        lines = []
                        for f in res:
                            lines.append('{0},{1},O,{2},{3},{4},{5}\n'.format(lastStage, txRow[1], f[1], frames[0][0], f[2], frames[0][1]))
                        fLog.writelines(lines)

                    
                    time.sleep(0.1) # wait short time to make sure all Frames from this stage are received
                    rxFrames = []
                    
                    while not q.empty():
                        rxFrames.append(q.get(False, 1000))
                    lines = []
                    for frame in rxFrames:
                        res = multiRawRx.convert_binary(frame)
                        lines.append('{0},{1},I,{2},{3},{4},{5}\n'.format(lastStage, res[0], res[1], res[3], res[4], res[5]))
                    fLog.writelines(lines)
                    validateResult = False
                    # validate rx frames and expected frames
                    if len(rxFrames) == len(rxFramesExpected):
                        print('Number expected OK: {0} == {1}'.format(len(rxFrames), len(rxFramesExpected)))
                        validateResult = True
                    elif len(rxFrames) > len(rxFramesExpected) and len(rxFrames) == len(rxFramesExpected)+len(rxFramesOptional):
                        print('Number expected OK: {0} == {1}+{2}'.format(len(rxFrames), len(rxFramesExpected), len(rxFramesOptional)))
                        validateResult = True
                    else:
                        print('Number of expected frames Wrong: {0} : {1} / {2}'.format(len(rxFrames), len(rxFramesExpected), len(rxFramesOptional)))
                        for rx in rxFrames:
                            print(rx)

                    # stage finished
                    txFrames = []
                    rxFramesExpected = []
                    rxFramesOptional = []

            if stopRequest:
                break

            if len(row) == 7:
                if row[2].upper() == 'O':
                    txFrames.append(row)
                elif row[2].upper() == 'I':
                    rxFramesExpected.append(row)
                elif row[2].upper() == 'X':
                    rxFramesOptional.append(row)
                    
            lastStage = stage
            #             
            if lastStage.upper() == 'STOP':
                stopRequest = True

    fLog.close()
    fCsv.close()


    # all lines parsed, now stop rx queues and thread
    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)


if __name__ == "__main__":
    main(sys.argv)
