﻿#!python3.4
# get_hw_info.py
#
# This file is part of python sample package for the BROADWAY2 package.
#
# MIT License

# Copyright (c) 2013-2018 FibreCode GmbH <info@fibrecode.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import sys, os
import platform
import time, datetime

from broadway2 import Broadway


def get_hw_info():
    """list all connected hardware adapters and return dictionary"""
    res = {'Class': 'hwadapter.fc.1', 'Date': str(datetime.datetime.now()), 'AdapterList': []}

    # create adapter singleton
    base = Broadway()


    for adapter in base.enumerate():
        AdapterInfo = {}
        AdapterInfo['Base'] = {'FWName': adapter.FWName, 'Revision': adapter.FWRevision, 'SerialNumber': adapter.SerialNumber, 'USBVendorId': adapter.UsbVendorId, 'USBDeviceId': adapter.UsbProductId}
        AdapterInfo['Base']['Key'] = adapter.SiliconKey
        AdapterInfo['Base']['FriendlyName'] = adapter.FriendlyName
        AdapterInfo['Base']['Instance'] = adapter.Inst

        if adapter.UsbProductId == 0x8575:
            AdapterInfo['Base']['SerialNo'] = adapter.SerialNoBase
            AdapterInfo['Base']['HwType'] = 'broadway'
            AdapterInfo['Base']['BoardId'] = 'FC011'
            AdapterInfo['Base']['Crc'] = 0
            AdapterInfo['Base']['MACAddr'] = '{0}'.format(adapter.MACAddress)
            AdapterInfo['Base']['StickTimestamp'] = '{0}'.format(adapter.StickTime)

            AdapterInfo['AddOn'] = {'FWName': 'n/a', 'Revision': 0, 'SerialNumber': ''}
            AdapterInfo['AddOn']['PhyType'] = adapter.PhyType
            AdapterInfo['AddOn']['SerialNo'] = adapter.SerialNoAddOn
            res['AdapterList'].append(AdapterInfo)
            
        elif adapter.UsbProductId == 0x8577:
            AdapterInfo['Base']['SerialNo'] = adapter.SerialNoBase
            AdapterInfo['Base']['HwType'] = 'broadway'
            AdapterInfo['Base']['BoardId'] = adapter.SerialNumber[0:5]
            AdapterInfo['Base']['Crc'] = 0
            AdapterInfo['Base']['MACAddr'] = '{0}'.format(adapter.MACAddress)

            res['AdapterList'].append(AdapterInfo)

        elif adapter.UsbProductId == 0x8579: # FC611
            AdapterInfo['Base']['SerialNo'] = adapter.SerialNoBase
            AdapterInfo['Base']['HwType'] = 'broadway'
            AdapterInfo['Base']['BoardId'] = adapter.SerialNumber[0:5]
            AdapterInfo['Base']['Crc'] = 0
            AdapterInfo['Base']['MACAddr'] = '{0}'.format(adapter.MACAddress)
            AdapterInfo['Base']['StickTimestamp'] = '{0}'.format(adapter.StickTime)

            res['AdapterList'].append(AdapterInfo)

        elif adapter.UsbProductId == 0x857B: # FC602
            AdapterInfo['Base']['SerialNo'] = adapter.SerialNoBase
            AdapterInfo['Base']['HwType'] = 'broadway'
            AdapterInfo['Base']['BoardId'] = adapter.SerialNumber[0:5]
            AdapterInfo['Base']['Crc'] = 0
            AdapterInfo['Base']['MACAddr'] = '{0}'.format(adapter.MACAddress)

            res['AdapterList'].append(AdapterInfo)

        elif adapter.UsbProductId == 0x857D: # FC612
            AdapterInfo['Base']['SerialNo'] = adapter.SerialNoBase
            AdapterInfo['Base']['HwType'] = 'broadway'
            AdapterInfo['Base']['BoardId'] = adapter.SerialNumber[0:5]
            AdapterInfo['Base']['Crc'] = 0
            AdapterInfo['Base']['MACAddr'] = '{0}'.format(adapter.MACAddress)
            AdapterInfo['Base']['StickTimestamp'] = '{0}'.format(adapter.StickTime)

            res['AdapterList'].append(AdapterInfo)

        elif adapter.UsbProductId == 0x857F: # FC401
            AdapterInfo['Base']['SerialNo'] = adapter.SerialNoBase
            AdapterInfo['Base']['HwType'] = 'broadway'
            AdapterInfo['Base']['BoardId'] = adapter.SerialNumber[0:5]
            AdapterInfo['Base']['Crc'] = 0
            AdapterInfo['Base']['MACAddr'] = '{0}'.format(adapter.MACAddress)

            res['AdapterList'].append(AdapterInfo)
        else:
            print('--- unsupported adapter found!')
    return res


def main(args):
    res = get_hw_info()
    print('Get Hardware Info: {0} {1}'.format(res['Class'], res['Date']))
    print('-'*80)
    print('Connected Adapters: {0}'.format(len(res['AdapterList'])))
    print('-'*80)
    for adapter in res['AdapterList']:
        print('FWName: {0} Rev.:{1} Serial:{2}'.format(adapter['Base']['FWName'], adapter['Base']['Revision'], adapter['Base']['SerialNumber']))
        print('='*70)

        print('   Device Instance: {0}'.format(adapter['Base']['Instance']))
        print('   Friendly Name: {0}'.format(adapter['Base']['FriendlyName']))
        print('   Hardware Type: {0}'.format(adapter['Base']['HwType']))
        print('   VendorID: 0x{0:4X} DeviceID: 0x{1:4X}'.format(adapter['Base']['USBVendorId'], adapter['Base']['USBDeviceId']))
        print('   Key: {0}'.format(adapter['Base']['Key']))
        print('   BoardId: {0}'.format(adapter['Base']['BoardId']))
        
        #print('   FWCheck: {0}'.format(adapter['Base']['FWChk']))
        print('   CRC: {0}'.format(adapter['Base']['Crc']))
        print()
        try:
            print('   Serial: {0}'.format(adapter['Base']['SerialNo']))
            print('   MACAddress: {0}'.format(adapter['Base']['MACAddr']))
        except:
            pass

        try:
            tm = int(adapter['Base']['StickTimestamp']) # started at 0 when powered-up
            tmStart = datetime.datetime.now() - datetime.timedelta(seconds=tm/1e9)
        except:
            tm = -1 # no StickTime
            pass
        if tm > 0:
            print('   StickTime: {0} s since: {1}'.format(tm / 1e9, tmStart))

        try:
            addOn = adapter['AddOn']
            print('      Add-On Board:')
        except:
            pass
        try:
            print('        PhyType: {0}'.format(adapter['AddOn']['PhyType']))
        except:
            pass

        try:
            print('        IDPROM: {0}'.format(adapter['AddOn']['IDProm']['Magic']))
            print('        Hardware Type: {0}'.format(adapter['AddOn']['HWType']))
        except:
            pass

        try:
            print('        Serial Number: {0}'.format(adapter['AddOn']['SerialNo']))
        except:
            pass
        print()


if __name__ == "__main__":
    main(sys.argv)
