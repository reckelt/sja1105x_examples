#!python3.4
# raw_send_receive.py
# Copyright (C) 2017 FibreCode GmbH
#
# This module is part of FibreCode software packages and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php 
'''
Date: 2017-11-09
Description: FC611 Raw Send-Receive
OS: requires Python 3.x
Requires: min. 2 x FC611 connected, Use 0 as Sender, 1 as receiver (only broadcasts to simplify usage)
Latest Comment: created
'''

import sys, os
import binascii
import platform
import time, datetime
import threading, queue

from pypacker.layer12 import arp, ethernet, ieee80211, prism
from pypacker.layer3 import ip, icmp
from pypacker.layer4 import udp, tcp
from pypacker.layer567 import ptpv2

from broadway2 import Broadway, broadway_api

SenderName = 'sw1p5'


def main(args):
    
    # create adapter singleton
    base = Broadway()

    if base.Count < 1:
        print('no FC611 connected!')
        sys.exit(-1)

    print('Connected Adapters:')
    print('-'*20)
    connectedAdapters = {}
    for a in base.enumerate():
        print('Adapter: {0} --> {1}'.format(a.Inst, a.FriendlyName))
        connectedAdapters[a.FriendlyName] = a.Inst
    print()
  
    try:
        fc611Send = base.get_adapter(connectedAdapters[SenderName])
    except:
        print('FC611 / {0} not connected'.format(SenderName))
        sys.exit(-2)
    

    devTx = base.open_raw_tx(fc611Send.Inst)
    

    print('Current Setup:')
    print('-'*20)
    print('FC611 Sender  : Name: {0:8} / {1}'.format(fc611Send.FriendlyName, fc611Send.MACAddress))


    # create custom packets and concat them
    packet1 = ethernet.Ethernet(dst_s="aa:bb:cc:dd:ee:ff", src_s="11:22:33:44:55:66") +\
	ip.IP(src_s="10.10.10.1", dst_s="10.10.10.2") +\
	icmp.ICMP(type=8) +\
	icmp.ICMP.Echo(id=1, ts=123456789, body_bytes=b"12345678901234567890")
    

    binData = packet1.bin()

    res = broadway_api.wait_for_tx(devTx, 1000)

    frameId = broadway_api.submit_raw_frame(devTx, binData)

    res = broadway_api.send_raw_frames(devTx)

    res = broadway_api.wait_for_raw_tx_completion(devTx, frameId, 1000)
    

    packet2 = ethernet.Ethernet(dst_s="aa:bb:cc:dd:ee:ff", src_s="11:22:33:44:55:66") +\
        ptpv2.PTPv2(type=ptpv2.PTPv2_TYPE_ANNOUNCE)
   
    print(packet2.hexdump())
    binData = packet2.bin()

    frameId = broadway_api.submit_raw_frame(devTx, binData)

    res = broadway_api.send_raw_frames(devTx)

    res = broadway_api.wait_for_raw_tx_completion(devTx, frameId, 1000)

    base.close(devTx)



if __name__ == "__main__":
    main(sys.argv)
