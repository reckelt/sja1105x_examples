#!python3.7
# fc_control.py

#
# This file is part of python sample package for the BROADWAY2 package.
#
# MIT License

# Copyright (c) 2013-2018 FibreCode GmbH <info@fibrecode.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys, os
import platform
import time, datetime
import json
import operator

from tkinter import *
from tkinter import ttk, filedialog, simpledialog

from broadway2 import Broadway

AppVersion = '2.2.0'    # added support for FC401

FontNormal = ('Arial', 10)
FontLarge = ('Arial', 12)
FontHuge = ('Arial', 18)


class App:
    
    def __init__(self, width=0, height=0):
        
        
        self.root = Tk()

        self.root.geometry
        self.root.grid(widthInc=width,heightInc=height)

        #Root-Window Apperace

        self.root.configure(background='#8899AA')
        self.root.title('FC-Control ' + AppVersion)
        self.root.resizable(width=FALSE, height=FALSE)

        #Call Function to Create the Slave Widgets

        self.createWindow()

        #Event Bind

        self.root.bind('<Destroy>',self.shutdown)

        
        self.base = Broadway() # create adapter singleton
        self.dev = None
        self.fca = None
        self.adapterDict = {}
        
        self.base.set_notify(self.notify_adapter_changed)

        #Mainloop
        self.root.mainloop()

    def createWindow(self):
        """
        ----------------------------------------------------------------
         createWindow(self)

          Function: Build the Slave Widgets of the Root  - Window
        -----------------------------------------------------------------
        """
       
        #Adapter - Label

        self.topFrame = Frame(self.root)
        self.topFrame.grid(column=0,row=0,sticky=W)

        lb = Label(self.topFrame, font = FontNormal, text = 'Connected Broadway2 Adapters: ')
        lb.configure(background='#FCFCFC')
        lb.grid(column=0,row=0,sticky=W)

        btnExport = Button(self.topFrame, font=FontNormal, text='Export Setup...', command=self.btnExport_clicked)
        btnExport.grid(column=1, row=0, sticky=W)

        self.btnEditMAC = Button(self.topFrame, font=FontNormal, text='Change MAC-Address ...', command=self.btnEditMAC_clicked)
        self.btnEditMAC.grid(column=2, row=0, sticky=W)
        self.btnEditMAC.config(state = DISABLED)

        self.btnEditName = Button(self.topFrame, font=FontNormal, text='Change FriendlyName ...', command=self.btnEditName_clicked)
        self.btnEditName.grid(column=3, row=0, sticky=W)
        self.btnEditName.config(state = DISABLED)

        self.bodyFrame = Frame(self.root)
        self.bodyFrame.grid(column=0,row=1,sticky=W)

        tree = self.tree = ttk.Treeview(self.bodyFrame, height=10)

        treeYScroll = ttk.Scrollbar(self.bodyFrame, orient="vertical",command=tree.yview)
        tree.configure(yscrollcommand=treeYScroll.set)

        tree.bind('<<TreeviewSelect>>', self.tree_selected)


        tree['columns']=('inst', 'sn', 'mac', 'fn', 'fw', 'pt')
        tree.column(0, width=40 )
        tree.column('inst', width=55 )
        tree.column('sn', width=100 )
        tree.column("mac", width=100)
        tree.column("fn", width=100)
        tree.column("pt", width=80)
        tree.column("fw", width=100)
        tree.heading("inst", text='Instance')
        tree.heading("sn", text='Serialnumber')
        tree.heading('mac', text='MAC-Address')
        tree.heading('fn', text='Friendly-Name')
        tree.heading('fw', text='FW-Version')
        tree.heading('pt', text='PhyType')

        # create base dirs for supported broadway2 adapters: FC611, FC602 and FC612
        self.idFC601 = tree.insert("", 'end', 'FC601', text='FC601 (not supported)')
        self.idFC401 = tree.insert("", 'end', 'FC401', text='FC401')
        self.idFC611 = tree.insert("", 'end', 'FC611', text='FC611')
        self.idFC602 = tree.insert("", 'end', 'FC602', text='FC602')
        self.idFC612 = tree.insert("", 'end', 'FC612', text='FC612')
        

        tree.grid(column=0,row=1,sticky=W)
        treeYScroll.grid(column=2,row=1)


    # EVENT Handler
    def shutdown(self, event):
        """close all open instances to clean-up"""
        pass

    def notify_adapter_changed(self, event):
        self.update_gui()

    def update_gui(self):
        for item in self.tree.get_children(self.idFC611):
            self.tree.delete(item)

        for item in self.tree.get_children(self.idFC602):
            self.tree.delete(item)

        for item in self.tree.get_children(self.idFC612):
            self.tree.delete(item)

        for item in self.tree.get_children(self.idFC401):
            self.tree.delete(item)

        self.adapterDict = {}
        for adapter in self.base.enumerate():
            colValues = ['{0}'.format(adapter.Inst),'{0}'.format(adapter.SerialNoBase), '{0}'.format(adapter.MACAddress), adapter.FriendlyName, adapter.FWRevision, adapter.PhyType]
            tagsValue = str(adapter.Inst)
            self.adapterDict[adapter.Inst] = adapter
            if adapter.UsbProductId == 0x8579:  # FC611
                self.tree.insert(self.idFC611, 'end', text='FC611-{0}'.format(adapter.Inst), values=colValues, tags=tagsValue)
            elif adapter.UsbProductId == 0x857B:  # FC602
                self.tree.insert(self.idFC602, 'end', text='FC602-{0}'.format(adapter.Inst), values=colValues, tags=tagsValue)
            elif adapter.UsbProductId == 0x857D:  # FC612
                self.tree.insert(self.idFC612, 'end', text='FC612-{0}'.format(adapter.Inst), values=colValues, tags=tagsValue)
            elif adapter.UsbProductId == 0x857F:  # FC401
                self.tree.insert(self.idFC401, 'end', text='FC401-{0}'.format(adapter.Inst), values=colValues, tags=tagsValue)

    def btnExport_clicked(self):
        saveFile = filedialog.asksaveasfilename(defaultextension='.csv', filetypes=[('Comma separated', '.csv'), ('Json structured', '.json')])
        if saveFile != '':
            print('saving ...', saveFile)
            self.export_settings(saveFile)

    def btnEditName_clicked(self):
        selected = self.tree.item(self.tree.focus())
        #print(selected)
        inst=int(selected['text'].split('-')[1])
        newName = simpledialog.askstring(title='New FriendlyName', prompt='Enter new name:', initialvalue=selected['values'][2])
        if (newName != None) and (newName != selected['values'][2]):
            dev = self.base.open_control(inst)
            self.fca = self.adapterDict[inst]
            self.fca.connect(dev)
            self.fca.set_friendly_name(newName)
            self.base.close(dev)
            self.base.scan_and_update()
            self.update_gui()

    def btnEditMAC_clicked(self):
        selected = self.tree.item(self.tree.focus())
        #print(selected)
        inst=int(selected['text'].split('-')[1])
        newMAC = simpledialog.askstring(title='Override MAC-Address', prompt='Enter new MAC-Address (00 for default):', initialvalue=selected['values'][1])
        if newMAC == None:
            return
        if newMAC == '00':
            dev = self.base.open_control(inst)
            self.fca = self.adapterDict[inst]
            self.fca.connect(dev)
            self.fca.use_mac_address_override(False)
            self.fca.reset_hw()
            self.base.close(dev)
            self.base.scan_and_update()
            self.update_gui()
        else:
            if len(newMAC) == 12:
                dev = self.base.open_control(inst)
                self.fca = self.adapterDict[inst]
                self.fca.connect(dev)
                self.fca.set_mac_address_override(newMAC)
                self.fca.use_mac_address_override(True)
                self.fca.reset_hw()
                self.base.close(dev)
                self.base.scan_and_update()
                self.update_gui()


    def tree_selected(self, event):
        selected = self.tree.item(self.tree.focus())
        try:
            adapterInstName = selected['text']
            inst = int(adapterInstName.split('-')[1])
            #print(adapterInstName, inst)
            self.btnEditName.config(state = ACTIVE)
            self.btnEditMAC.config(state = ACTIVE)
            self.twinkle(inst)
        except:
            self.btnEditName.config(state = DISABLED)
            self.btnEditMAC.config(state = DISABLED)


    def export_settings(self, fileName):
        """supported are csv and json automatically selected by given file-extension"""
        try:
            ext = os.path.splitext(fileName)[1].lower()
        except:
            ext = ''

        if ext == '.csv':
            f = open(fileName, 'w')
            lineOut = '{0};{1};{2}\n'.format('Serialnumber', 'MAC-Address', 'Friendly-Name', 'FWVersion', 'PhyType')
            for adapter in self.base.enumerate():
                lineOut = '{0};{1};{2};{3}\n'.format(adapter.MACAddress, adapter.SerialNoBase, adapter.FriendlyName, adapter.FWRevision, adapter.PhyType)
                f.write(lineOut)
            f.close()

            
        elif ext == '.json':
            pass

    def twinkle(self, inst):
        if self.dev == None:
            self.dev = self.base.open_control(inst)
            self.fca = self.adapterDict[inst]
            self.fca.connect(self.dev)
            self.fca.twinkle_start(3)
            self.root.after(2000, self.twinkle, inst)
        else:
            self.fca.twinkle_stop()
            self.fca.disconnect()
            self.base.close(self.dev)
            self.fca = None
            self.dev = None






def main(args):

    # will never return until gui exits
    app = App(80, 50)

if __name__ == "__main__":
    main(sys.argv)
