#!python3.4
# raw_send_receive.py
# Copyright (C) 2017 FibreCode GmbH
#
# This module is part of FibreCode software packages and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php 
'''
Date: 2017-11-09
Description: FC611 Raw Send-Receive
OS: requires Python 3.x
Requires: min. 2 x FC611 connected, Use 0 as Sender, 1 as receiver (only broadcasts to simplify usage)
Latest Comment: created
'''

import sys, os
import binascii
import platform
import time, datetime
import threading, queue

from broadway2 import broadway_api, Broadway, EthRawFrame

class Sender(threading.Thread):
    def __init__(self, dev, q, e):
        threading.Thread.__init__(self)
        self.e = e 
        self.dev = dev
        self.inQ = q

    def run(self):
        while True:
            print('Sender running and waiting...')
            if self.e.wait(0):
                print('Sender got stop signal!')
                break
            try:
                res = self.inQ.get(True, 1)
                if res != None:
                    ethRaw = EthRawFrame(res)
                    print('Sender received raw: {0}'.format())
                    

                    res = broadway_api.wait_for_tx(self.dev, 1000)
                    frameId = broadway_api.submit_raw_frame(self.dev, binData)
                    print('frameId: {0}'.format(frameId))
                    res = broadway_api.send_raw_frames(self.dev)

                    res = broadway_api.wait_for_raw_tx_completion(self.dev, frameId, 1000)
            except:
                pass


class Receiver(threading.Thread):
    def __init__(self, dev, q, e):
        threading.Thread.__init__(self) 
        self.dev = dev
        self.outQ = q
        self.stopEvent = e

    def run(self):
        while True:
            if self.stopEvent.wait(0):
                print('Receiver got stop signal!')
                break

            res = broadway_api.wait_raw_rx_frame(self.dev, 5000)
            if res:
                print('Receiver triggered!')
                res = broadway_api.get_raw_rx_frame(self.dev, 1000)
                if res != None:
                    ethRaw = EthRawFrame(res)
                    
                    print('   Len:{0}    {1}...'.format(len(res), ethRaw.EthFrame[0:28]))
                    self.outQ.put(res)
            


def main(args):
    
    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    fc611Send = base.get_adapter(0)
    fc611Recv = base.get_adapter(1)

    devTx = base.open_raw_tx(0)
    devRx = base.open_raw_rx(1, broadway_api.RawRxHeaderType.Type2)

    stopR = threading.Event()
    stopT = threading.Event()

    qIn = queue.Queue()
    qOut = queue.Queue()


    res = broadway_api.enable_raw_rx(devRx)

    sendThread = Sender(devTx, qIn, stopT)
    recvThread = Receiver(devRx, qOut, stopR)

    sendThread.start()
    recvThread.start()


    qIn.put('AABBCCDDEEFF11223344556608000053AFFE0102030405060708090A' + 'ABCD'*24 + 'AFFE')    # ethernet raw frame
    time.sleep(0.1)
    qIn.put('AABBCCDDEEFF11223344556608000053AFFE0102030405060708090A' + 'ABCD'*24 + 'AFFE')    # ethernet raw frame
    qIn.put('FFFFFFFFFFFF11223344556608000053AFFE' + '0102030405060708090A112233445566778899AA112233445566778899AA' * 50 + 'AFFEBEEF')    # max ethernet raw frame

    time.sleep(5)

    stopT.set()
    stopR.set()

    sendThread.join(500)
    recvThread.join(500)


if __name__ == "__main__":
    main(sys.argv)
