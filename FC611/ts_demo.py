#python3.7

# Verify hwtimestamp on FC611

import sys, os
import platform
import time, datetime

from broadway2 import Broadway, PhyTJA1100


def main(args):
    # create adapter singleton
    base = Broadway()

    rawList = base.get_raw_adapters()

    if len(rawList) > 0:
        adapterInst = rawList[0][0]
        print('Use {0}'.format(rawList[0][1]))

        dev = base.open_control(adapterInst)

        adapter = base.get_adapter(adapterInst)

        adapter.connect(dev)

        tm = adapter.get_timestamp()
        ts1 = tm[0]*1e9+tm[1]
        tm = adapter.get_timestamp()
        ts2 = tm[0]*1e9+tm[1]
        print('TS1: {0} TS2:{1}  Diff:{2} ns'.format(ts1, ts2, ts2-ts1))

        base.close(dev)



if __name__ == "__main__":
    main(sys.argv)
