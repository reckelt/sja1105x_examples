#!python3.4
# raw_send_receive.py
# Copyright (C) 2017 FibreCode GmbH
#
# This module is part of FibreCode software packages and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php 
'''
Date: 2017-11-09
Description: FC611 Raw Send-Receive
OS: requires Python 3.x
Requires: min. 2 x FC611 connected, Use 0 as Sender, 1 as receiver (only broadcasts to simplify usage)
Latest Comment: created
'''

import sys, os
import binascii
import platform
import time, datetime
import threading, queue

from broadway2 import Broadway, broadway_api, EthRawFrame

SenderName = 'silver'
ReceiverName = 'yellow'


def main(args):
    
    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Connected Adapters:')
    print('-'*20)
    connectedAdapters = {}
    for a in base.enumerate():
        print('Adapter: {0} --> {1}'.format(a.Inst, a.FriendlyName))
        connectedAdapters[a.FriendlyName] = a.Inst
    print()
 
    try:
        fc611Send = base.get_adapter(connectedAdapters[SenderName])
    except:
        print('FC611 / {0} not connected'.format(SenderName))
        sys.exit(-2)
    try:
        fc611Recv = base.get_adapter(connectedAdapters[ReceiverName])
    except:
        print('FC611 / {1} not connected'.format(ReceiverName))
        sys.exit(-2)

    devTx = base.open_raw_tx(fc611Send.Inst)
    devRx = base.open_raw_rx(fc611Recv.Inst, broadway_api.RawRxHeaderType.Type2)

    fc611Send.connect(devTx)
    fc611Recv.connect(devRx)
    print('Current Setup:')
    print('-'*20)
    print('FC611 Sender  : Name: {0:8} / {1}'.format(fc611Send.FriendlyName, fc611Send.MACAddress))
    print('FC611 Receiver: Name: {0:8} / {1}'.format(fc611Recv.FriendlyName, fc611Recv.MACAddress))

    print()
    # use broadway-API for send/receive
    res = broadway_api.enable_raw_rx(devRx)

    fc611Send.init_timestamp(0,0)
    fc611Recv.init_timestamp(0,0)

    #frameData = 'FFFFFFFFFFFF11223344556688A8007381000053AFFE0102030405060708090A'
    sendData = 'FFFFFFFFFFFF11223344556608000053AFFE0102030405060708090A' + '{0:04X}'.format(1000)
    #sendData = 'AABBCCDDEEFF11223344556608000053AFFE0102030405060708090A' + 'ABCD'*24 + 'AFFE'
    
    binData = binascii.a2b_hex(sendData)

    res = broadway_api.wait_for_tx(devTx, 1000)
   
    frameId1 = broadway_api.submit_raw_frame(devTx, binData)

    sendData = 'FFFFFFFFFFFF11223344556608000053AFFE' + '0102030405060708090A112233445566778899AA112233445566778899AA' * 20 + 'AFFEBEEF'
    binData = binascii.a2b_hex(sendData)

    res = broadway_api.wait_for_tx(devTx, 1000)
    frameId2 = broadway_api.submit_raw_frame(devTx, binData)
    res = broadway_api.send_raw_frames(devTx)

    res = broadway_api.wait_for_raw_tx_completion(devTx, frameId2, 1000)

    tm1 = broadway_api.get_raw_tx_timestamp(devTx, frameId1)
    tm2 = broadway_api.get_raw_tx_timestamp(devTx, frameId2)
    
    print('sent: Frame1:{0} NumBytes: {1} (Time: {2}.{3})'.format(frameId1, len(binData), tm1[0], tm1[1]))

    print('sent: Frame2:{0} NumBytes: {1} (Time: {2}.{3})'.format(frameId2, len(binData), tm2[0], tm2[1]))
    
    cnt = 5
    while cnt > 0:           
        res = broadway_api.wait_raw_rx_frame(devRx, 1000)
        if res:
            res = broadway_api.get_raw_rx_frame(devRx, 100)
            if res != None:
                ethRaw = EthRawFrame(res)
                print('Received: Timestamp: {0}.{1}  SeqCnt: {2}'.format(ethRaw.TimestampSec, ethRaw.TimestampNano, ethRaw.SequenceCnt))

                eth = ethRaw.EthFrame

                print('    Dest: {0:08X}  Src: {1:08X} Type:{2:04X} DataLen:{3}'.format(eth.TgtMac, eth.SrcMac, eth.EtherType, len(eth.Data)))
                print('    Data: {0}'.format(binascii.b2a_hex(eth.Data[0:16])))
        cnt -= 1

    fc611Send.disconnect()
    fc611Recv.disconnect()

    base.close(devTx)
    base.close(devRx)



if __name__ == "__main__":
    main(sys.argv)
