#!python3.4
# get_hw_info.py
# Copyright (C) 2013-2014 FibreCode GmbH
#
# This module is part of FibreCode software packages and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php 
'''
Date: 2017-11-06
Description: Demo to read all connected phys
OS: requires Python 3.x
Requires: USB-OABR Adapter
Latest Comment:
'''
import sys, os
import platform
import time, datetime

from broadway2 import Broadway
from broadway2 import get_phy_access


def main(args):
    # create adapter singleton
    base = Broadway()


    regs = [1,2,3,17,23]

    for adapter in base.enumerate():
        print('Serial:{0} Name:{1}'.format(adapter.SerialNoBase, adapter.FriendlyName))
        print('='*75)
       
       
        dev = base.open_control(adapter.Inst)
        adapter.connect(dev)

        phyAccess = get_phy_access(adapter)

        for reg in regs:
            value = phyAccess.read_reg(reg)
            print('Reg[{0:02X}] = {1:08X}'.format(reg, value))

        adapter.disconnect()

        base.close(dev)
        print()


if __name__ == "__main__":
    main(sys.argv)
