#!python3.4
# raw_receiver_gui.py
# Copyright (C) 2017 FibreCode GmbH
#
# This module is part of FibreCode software packages and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php 
'''
Date: 2017-11-09
Description: FC611 Raw Send-Receive
OS: requires Python 3.x
Requires: FC611 connected, Use 0 as Sender, 1 as receiver (only broadcasts to simplify usage)
Latest Comment: created
'''

import sys, os
import binascii
import platform
import time, datetime
import threading, queue

from broadway2 import Broadway, broadway_api

class Receiver(threading.Thread):
    def __init__(self, adapter, q, e):
        threading.Thread.__init__(self) 
        self.adapter = adapter
        self.devRx = None
        self.outQ = q
        self.stopEvent = e

    def connect(self):
        self.devRx = base.open_raw_rx(self.adapter.Inst, broadway_api.RawRxHeaderType.Type1)
        res = broadway_api.enable_raw_rx(self.devRx)

    def disconnect(self):
        res = broadway_api.disable_raw_rx(self.devRx)


    def run(self):
        while True:
            if self.stopEvent.wait(0):
                print('Receiver got stop signal!')
                break

            res = broadway_api.wait_raw_rx_frame(self.dev, 5000)
            if res:
                print('Receiver triggered!')
                res = broadway_api.get_raw_rx_frame(self.dev, 1000)
                if res != None:
                    timeReceived = int.from_bytes(res[0:8], byteorder='little')
                    seqNumber = int.from_bytes(res[8:12], byteorder='little')
                    print('Received: Timestamp: {0}  SeqCnt: {1}'.format(timeReceived, seqNumber))
                    frameData = binascii.b2a_hex(res[12:])
                    print('   Len:{0}    {1}...'.format(len(res), frameData[0:28]))
                    self.outQ.put(res)
            



def main(args):

    if len(args) < 1:
        ReceiverName = 'red'    
    else:
        ReceiverName = args[1]

    # create adapter singleton
    base = Broadway()

    if base.Count < 1:
        sys.exit(-1)

    print('Connected Adapters:')
    print('-'*20)
    connectedAdapters = {}
    for a in base.enumerate():
        print('Adapter: {0} --> {1}'.format(a.Inst, a.FriendlyName))
        connectedAdapters[a.FriendlyName] = a.Inst
    print()

    try:
        fc611Recv = base.get_adapter(connectedAdapters[ReceiverName])
    except:
        print('FC611 / {0} not connected'.format(ReceiverName))
        sys.exit(-2)


    stopR = threading.Event()

   
    print()
   

    base.close(devTx)
    



if __name__ == "__main__":
    main(sys.argv)
