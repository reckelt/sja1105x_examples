# README #

README for SJA1105x Examples

### What is this repository for? ###

* Python samples to work with NXP SJA1105x EvalBoards
* Version 0.5

### Requirements ###

* SJA1105T (AM3) Board or SJA1105Q Evalboard
* Python 3.4 or higher
* Dependencies
	+ public: pip install pyserial intelhex prettytable
		- pyserial
		- intelhex
		- prettytable
	+ private: python setup.py install (for each of the following)
		- SJA1105
		- SJA1105_Platform
		- broadway (FC601)
		- broadway2 (FC611, FC602)

### Setups ###

* SJA1105T (AM3) and 3 x FC601
	- Windows 10 Notebook
	- 2 x SBCs (Single Board Computer running Linux) like i.MX6 or Raspberry Pi
	- Wireshark installed

	- FC601 on each SBC
	- Connect FC601 to PC
	
* SJA1105Q Evalboard and 4 x FC611
	- Windows 10 Notebook
	- Connected FC611 to PC using USB-Hub
	- Connected SJA1105Q EvalBoard also on USB-Hub
	- All OABR-Ports of SJA1105Q EVB connected to FC611


### Contents ###

* Full blown configurations and matching test-setup

### Who do I talk to? ###

* Validation and verification teams all over the world
* Use simple and powerful python environment to configure and verify data flow of NXP SJA1105 designs (SJA1105, SJA1105T, SJA1105PQRS)