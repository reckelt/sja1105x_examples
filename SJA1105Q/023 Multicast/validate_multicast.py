import os, sys
import logging
import time
import binascii
import threading, queue

from broadway2 import Broadway, broadway_api, EthFrame, VLANType, FCAdapter, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

# Silver: FC0011111111
# Yellow: FC0022222222
# Orange: FC0033333333
# Red   : FC0044444444

#
#   SJA1105Q-EVB: Port 0 internal to LPC
#
#            SJA1105Q
#
#       TJA1102     TJA1102 
#
#   Ports:  3   4   1   2
#
#   Addr:  14  15   8   9
#   LinkP: or  re   si  ye

# Black-listed Filters:
#   FC0033333333


def main(args):
    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():        
        if a.DevName == 'FC611':
            print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
            devTx = base.open_raw_tx(a.Inst)

            multiRawTx.add(a, devTx)

            devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

            multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()

    payload = 128
    data = ''
    for j in range(0, payload):
        data += '{0:02X}'.format(j)

    llcType = '{0:04X}'.format(payload + 7)
    llcHead = '1234'


    frameListSilver = [
            ('FFFFFFFFFFFF', llcType, llcHead + 'FC00110001' + data)
    ]
    frameListYellow = [
            ('0108C200000E', llcType, llcHead + 'FC00110001' + data)    ]


    
    ExpectedResult = {'red': 2, 'silver': 1, 'yellow': 1, 'orange':2}


    res = multiRawTx.send_raw_frames('silver', '', frameListSilver)
    res = multiRawTx.send_raw_frames('yellow', '', frameListYellow)

    # wait short time until all frames are received 
    time.sleep(1)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)

    # make sure not to validate too early!
    for dev in rxFrames:
        print('Dev: {0} Frames: {1}'.format(dev, len(rxFrames[dev])))
        for rxFrame in rxFrames[dev]:
            eth = RawRxFrame(1, rxFrame[1])
            print(rxFrame[0], eth.get_timestamp_ns(), hex(eth.EthFrame.SrcMac), hex(eth.EthFrame.TgtMac))

# End of Send/Receive reached.


# Validation will start here!

    ErrorCnt = 0
    for dev in ExpectedResult:
        try:
            numRxFrames = len(rxFrames[dev])
        except:
            numRxFrames = 0

        if numRxFrames != ExpectedResult[dev]:
            print('Validation Count Error in: {0} {1}!={2}'.format(dev, numRxFrames, ExpectedResult[dev]))
            ErrorCnt += 1
        else:
            print('{0:15} received {1}: OK'.format(dev, numRxFrames))
        

    if ErrorCnt > 0:
        print('?'*48)
        print('Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('Validation OK')
        print('-'*48)

# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)
    


if __name__ == "__main__":
    main(sys.argv)

