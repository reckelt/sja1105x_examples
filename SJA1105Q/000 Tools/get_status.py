# Python3.x
import os, sys
import time

from sja1105_platform import SJA1105Platform, SJA1105PlatformDefs, PlatformHelper
from sja1105 import SJA1105PCGU, SJA1105

from broadway2 import Broadway, broadway_api, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame


hlp = PlatformHelper()
hlp.detect()

if len(hlp.platformPorts) != 1:
    print('make sure to connect only one EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0) # we only support 1 connected board here

try:
    platform = SJA1105Platform(port)    # w
except Exception as e:
    print(e)
    sys.exit(-1)

print('START: {0}'.format(port.port))
currentPlatformVersion = platform.get_version()

print('uC SW version:   {0:08X}'.format(currentPlatformVersion))    # read the microcontroller software version
print('DeviceId:        {0:08X}'.format(platform.spi_read(0)))      # read the deviceId
print('Status register: {0:08X}'.format(platform.spi_read(1)))      # read the status register


PllRegs = {0x100007, 0x100008, 0x100009, 0x10000A, 0x100027, 0x100029}
for reg in PllRegs:
    pllVal = platform.spi_read(reg)
    print('PLL-Register: {0:04X} = {1:04X}'.format(reg, pllVal))

StatRegs = {0x400, 0x401, 0x402, 0x410, 0x411, 0x412, 0x420, 0x430, 0x440}

sum = 0
for reg in StatRegs:
    stat = platform.spi_read(reg)
    sum += stat
    print('Stat-Counter: {0:04X} = {1}'.format(reg, stat))



hlp.close(port)


