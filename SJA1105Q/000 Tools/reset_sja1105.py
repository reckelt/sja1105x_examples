# Python3.x
import os, sys
import time

from sja1105_platform import SJA1105Platform, SJA1105PlatformDefs, PlatformHelper
from sja1105 import SJA1105PCGU, SJA1105

from broadway2 import Broadway, broadway_api, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame


hlp = PlatformHelper()
hlp.detect()

if len(hlp.platformPorts) != 1:
    print('make sure to connect only one EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0) # we only support 1 connected board here

try:
    platform = SJA1105Platform(port)    # w
except Exception as e:
    print(e)
    sys.exit(-1)

print('START: {0}'.format(port.port))
currentPlatformVersion = platform.get_version()

print('uC SW version:   {0:08X}'.format(currentPlatformVersion))    # read the microcontroller software version
print('DeviceId:        {0:08X}'.format(platform.spi_read(0)))      # read the deviceId
print('Status register: {0:08X}'.format(platform.spi_read(1)))      # read the status register


platform.reset()

print('Status register: {0:08X}'.format(platform.spi_read(1)))      # read the status register


hlp.close(port)



