# Python3.x
import os, sys
import time

from sja1105_platform import SJA1105Platform, SJA1105PlatformDefs, PlatformHelper
from sja1105 import SJA1105PCGU


HexFile = os.path.join(os.path.split(__file__)[0], 'config_policy_SJA1105QS.hex')


CGUSettingsQ = [

    # initialize PLL1 50MHz
    {'Reg': SJA1105PCGU.PLL_1_C, 'Value': 0x0A010941},
    {'Reg': SJA1105PCGU.PLL_1_C, 'Value': 0x0A010940},
    

    # port 0: RGMII/RMII (PHY-MODE) to ext. Host
    {'Reg': SJA1105PCGU.RMII_REF_CLK_0, 'Value':0x00000800},
    {'Reg': SJA1105PCGU.EXT_TX_CLK_0, 'Value': 0x0E000800},
    
    # port 1: MII (MAC-MODE)
    {'Reg': SJA1105PCGU.IDIV_1_C, 'Value': 0x0a000001},
    {'Reg': SJA1105PCGU.MII_TX_CLK_1, 'Value': 0x02000000},
    {'Reg': SJA1105PCGU.MII_RX_CLK_1, 'Value': 0x03000000},

    # port 2: MII (MAC-MODE)
    {'Reg': SJA1105PCGU.IDIV_2_C, 'Value': 0x0a000001},
    {'Reg': SJA1105PCGU.MII_TX_CLK_2, 'Value': 0x04000000},
    {'Reg': SJA1105PCGU.MII_RX_CLK_2, 'Value': 0x05000000},

    # port 3: RMII (MAC-MODE)
    {'Reg': SJA1105PCGU.IDIV_3_C, 'Value': 0x0a000001},
    {'Reg': SJA1105PCGU.RMII_REF_CLK_3, 'Value': 0x06000800},
    # EXT_TX_CLK to PLL1
    {'Reg': SJA1105PCGU.EXT_TX_CLK_3, 'Value': 0x0E000800},

    # port 4: RMII (MAC-MODE)
    {'Reg': SJA1105PCGU.IDIV_4_C, 'Value': 0x0a000001},
    {'Reg': SJA1105PCGU.RMII_REF_CLK_4, 'Value': 0x08000800},
    # EXT_TX_CLK to PLL1
    {'Reg': SJA1105PCGU.EXT_TX_CLK_4, 'Value': 0x0E000800},
]



hlp = PlatformHelper()
hlp.detect()

if len(hlp.platformPorts) != 1:
    print('make sure to connect only one EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0) # we only support 1 connected board here

try:
    platform = SJA1105Platform(port)    # w
except Exception as e:
    print(e)
    sys.exit(-1)

print('START: {0}'.format(port.port))
currentPlatformVersion = platform.get_version()

print('HexFile:         {0}'.format(HexFile))
print('uC SW version:   {0:08X}'.format(currentPlatformVersion))    # read the microcontroller software version
print('DeviceId:        {0:08X}'.format(platform.spi_read(0)))      # read the deviceId
print('Status register: {0:08X}'.format(platform.spi_read(1)))      # read the status register


print('-'*20)

print('reset')
platform.reset()

print('-'*20)


print('download {0}'.format(HexFile))

statusBefore = platform.spi_read(1)
print('Status register before: {0:08x}'.format(statusBefore))
# Configure Switch using binary-hex file
platform.spi_config(HexFile)

statusAfter = platform.spi_read(1)
print('Status register after:  {0:08X}'.format(statusAfter))


print('-'*20)

if (currentPlatformVersion == SJA1105PlatformDefs.SJA1105Q or currentPlatformVersion==SJA1105PlatformDefs.SJA1105Q_01):
    CGUSettings = CGUSettingsQ
else:
    print('UNSUPPORTED PLATFORM!!!')
    CGUSettings = None

if CGUSettings != None:
    for cguReg in CGUSettings:
        platform.spi_write(cguReg['Reg'], cguReg['Value'])

    print('CGU Settings written')
    print('-'*20)



hlp.close(port)

