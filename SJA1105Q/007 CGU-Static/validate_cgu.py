import os, sys
import logging
import time
import threading, queue

from sja1105_platform import SJA1105Platform, PlatformHelper
from broadway2 import Broadway, broadway_api, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

HWSetup = {'red': 3}


def main(args):

    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    fcRed = base.get_adapter_by_name('red')

    if fcRed == None:
        print('requires 1xFC611/FC612 named "red"')
        sys.exit(-1)

    # detect/connect SJA1105Q via SJA1105Q-EVB
    hlp = PlatformHelper()
    hlp.detect()

    if hlp.NumPlatformsFound != 1:
        print('requires 1x SJA1105Q-EVB')
        sys.exit(-1)

    port = hlp.open(hlp.platformPortMap['SJA1105Q_EVB']) 
    sja1105Evb = SJA1105Platform(port)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        if a.FriendlyName == 'red':
            print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
            devTx = base.open_raw_tx(a.Inst)

            multiRawTx.add(a, devTx)

            devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

            multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()
        
    time.sleep(0.1)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)

                    
    testResult = True
    print('-'*48)
    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))

    
    ptpTimeL1 = sja1105Evb.spi_read(0x1C)
    ptpTimeL2 = sja1105Evb.spi_read(0x1C)

    ptpTimeH1 = sja1105Evb.spi_read(0x1D)
    ptpTimeH2 = sja1105Evb.spi_read(0x1D)

    tm1 = ((ptpTimeH1 << 32) | ptpTimeL1) * 8e-9    # ticks are 8 ns
    tm2 = ((ptpTimeH2 << 32) | ptpTimeL2) * 8e-9    # ticks are 8 ns

    print('PTP-Time1 {0}'.format(tm1))

    
    print('PTP-Time2 {0}'.format(tm2))

    print('Diff: {0}'.format(tm2-tm1))

    sja1105Evb.spi_write(0x1B, 0x80000000)

    tmStampValid = sja1105Evb.spi_read(0xC0)
    tmStamp = sja1105Evb.spi_read(0xC1)

    

    time.sleep(0.5)

    # close SJA1105Q-EVB
    hlp.close(port)

# End of Send/Receive reached.


# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)


    


if __name__ == "__main__":
    main(sys.argv)

