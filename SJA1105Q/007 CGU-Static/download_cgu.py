# Python3.x
import os, sys
import time

from sja1105_platform import SJA1105Platform, SJA1105PlatformDefs, PlatformHelper
from sja1105 import SJA1105PCGU



HexFile = os.path.join(os.path.split(__file__)[0], 'config_cgu_SJA1105QS.hex')



hlp = PlatformHelper()
hlp.detect()

if len(hlp.platformPorts) != 1:
    print('make sure to connect exactly one EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0) # we only support 1 connected board here

try:
    platform = SJA1105Platform(port)    # w
except Exception as e:
    print(e)
    sys.exit(-1)

print('START: {0}'.format(port.port))
currentPlatformVersion = platform.get_version()

print('HexFile:         {0}'.format(HexFile))
print('uC SW version:   {0:08X}'.format(currentPlatformVersion))    # read the microcontroller software version
print('DeviceId:        {0:08X}'.format(platform.spi_read(0)))      # read the deviceId
print('Status register: {0:08X}'.format(platform.spi_read(1)))      # read the status register


print('-'*20)

print('reset')
platform.reset()

print('-'*20)


print('download {0}'.format(HexFile))

statusBefore = platform.spi_read(1)
print('Status register before: {0:08x}'.format(statusBefore))
# Configure Switch using binary-hex file
platform.spi_config(HexFile)

statusAfter = platform.spi_read(1)
print('Status register after:  {0:08X}'.format(statusAfter))


print('-'*20)



hlp.close(port)

