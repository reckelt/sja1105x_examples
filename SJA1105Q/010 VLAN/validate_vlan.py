import os, sys
import logging
import time
import binascii
import threading, queue

from broadway2 import Broadway, broadway_api, EthFrame, VLANType, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

# Silver: FC0011111111
# Yellow: FC0022222222
# Blue  : FC0033333333
# Red   : FC0044444444

#
#   SJA1105Q-EVB: Port 0 internal to LPC
#
#            SJA1105Q
#
#       TJA1102     TJA1102 
#
#   Ports:  3   4   1   2
#
#   Addr:  14  15   8   9
#   LinkP: bl  re   si  ye

#   VLAN     21      19



def main(args):
    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
        devTx = base.open_raw_tx(a.Inst)

        multiRawTx.add(a, devTx)

        devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

        multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()


    payload = 128
    data = ''
    for j in range(0, payload):
        data += '{0:02X}'.format(j)

    llcType = '{0:04X}'.format(payload + 7)
    llcHead = '1234'

    # Silver no VLAN
    frameListSilverNoVLAN = [
            ('FC0022222222', llcType, llcHead + 'FC00110001' + data),
            ('FC0033333333', llcType, llcHead + 'FC00110002' + data),
            ('FC0044444444', llcType, llcHead + 'FC00110003' + data)
    ]

    # VLAN 19 (Silver-Yellow)
    frameListSilverVLAN19 = [
            ('FC0022222222', '81000013' + llcType, llcHead + 'FC00110001' + data),
            ('FC0033333333', '81000013' + llcType, llcHead + 'FC00110002' + data),
            ('FC0044444444', '81000013' + llcType, llcHead + 'FC00110003' + data)
    ]

    frameListOrangeNoVLAN = [
            ('FC0011111111', llcType, llcHead + 'FC00330001' + data),
            ('FC0022222222', llcType, llcHead + 'FC00330002' + data),
            ('FC0044444444', llcType, llcHead + 'FC00330003' + data)
    ]

    # VLAN 21 (Blue-Red)
    frameListOrangeVLAN21 = [
            ('FC0011111111', '81000015' + llcType, llcHead + 'FC00330001' + data),
            ('FC0022222222', '81000015' + llcType, llcHead + 'FC00330002' + data),
            ('FC0044444444', '81000015' + llcType, llcHead + 'FC00330003' + data)
    ]

    # Double-VLAN 07.21
    frameListVLAN21 = [
            ('FC0022222222', '88A80007810000150800', '050100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]


    res = multiRawTx.send_raw_frames('silver', '', frameListSilverNoVLAN)

    res = multiRawTx.send_raw_frames('orange', '', frameListOrangeNoVLAN)

    res = multiRawTx.send_raw_frames('silver', '', frameListSilverVLAN19)

    res = multiRawTx.send_raw_frames('orange', '', frameListOrangeVLAN21)

    # wait short time until all frames are received 
    time.sleep(0.5)

    rxFrames = {}
    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)


    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))
        for rxFrame in rxFrames[dev]:
            ethRaw = RawRxFrame(1, rxFrame[1])
            print(ethRaw)

    time.sleep(0.8)

# End of Send/Receive reached.


# Validation will start here!

    ExpectedResult = {'red': 9, 'silver': 0, 'yellow': 3, 'blue':0}
    ErrorCnt = 0
    for dev in ExpectedResult:
        try:
            numRxFrames = len(rxFrames[dev])
        except:
            numRxFrames = 0

        if numRxFrames != ExpectedResult[dev]:
            print('Validation Count Error in: {0} {1}!={2}'.format(dev, numRxFrames, ExpectedResult[dev]))
            ErrorCnt += 1
        

    if ErrorCnt > 0:
        print('?'*48)
        print('Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('Validation OK')
        print('-'*48)




# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)
    


if __name__ == "__main__":
    main(sys.argv)

