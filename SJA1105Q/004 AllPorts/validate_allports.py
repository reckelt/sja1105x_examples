import os, sys
import logging
import time
import threading, queue

from broadway2 import Broadway, broadway_api, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

HWSetup = {'red': 3, 'blue': 2, 'yellow': 0, 'silver': 1}


def main(args):

    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
        devTx = base.open_raw_tx(a.Inst)

        multiRawTx.add(a, devTx)

        devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

        multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()

    frameListSilver = [
            ('FC0022222222', '0800', '020200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '020300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '020400010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]
    frameListYellow = [
            ('FC0011111111', '0800', '030100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '030300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '030400010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    frameListRed = [
            ('FC0011111111', '0800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '0800', '040200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '040300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    frameListBlue = [
            ('FC0011111111', '0800', '050100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '0800', '050200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '050400010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    ExpectedNumRxFrames = {'silver': len(frameListYellow)+ len(frameListBlue)+len(frameListRed),
                           'yellow': len(frameListSilver)+ len(frameListBlue)+len(frameListRed),
                           'blue': len(frameListSilver)+ len(frameListYellow)+len(frameListRed),
                           'red': len(frameListSilver)+ len(frameListBlue)+len(frameListYellow)
                           }


# Here, we can now start to send data between ports

    res = multiRawTx.send_raw_frames('silver', '', frameListSilver)
    time.sleep(0.1)
    res = multiRawTx.send_raw_frames('yellow', '', frameListYellow)
    time.sleep(0.1)
    res = multiRawTx.send_raw_frames('red', '', frameListRed)
    time.sleep(0.1)
    res = multiRawTx.send_raw_frames('blue', '', frameListBlue)
    
    time.sleep(0.5)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)
                    
    testResult = True
    print('-'*48)
    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))
        if ExpectedNumRxFrames[dev] != len(rxFrames[dev]):
            print('Expectation failed: {0}'.format(dev))
            testResult = False


    print('Result for learning OFF: {0}'.format(testResult))

# End of Send/Receive reached.

    ExpectedResult = {'red': 9, 'silver': 9, 'yellow': 9, 'blue':9}
    ErrorCnt = 0

    for dev in ExpectedResult:
        try:
            numRxFrames = len(rxFrames[dev])
        except:
            numRxFrames = 0

        if numRxFrames != ExpectedResult[dev]:
            print('Validation Error in: {0} {1}!={2}'.format(dev, numRxFrames, ExpectedResult[dev]))
            ErrorCnt += 1


    if ErrorCnt > 0:
        print('?'*48)
        print('Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('Validation OK')
        print('-'*48)



# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)


if __name__ == "__main__":
    main(sys.argv)

