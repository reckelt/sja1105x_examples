import os, sys
import logging
import time
import binascii
import threading, queue

from broadway2 import Broadway, broadway_api, EthFrame, VLANType, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

# Silver: FC0011111111
# Yellow: FC0022222222
# Blue  : FC0033333333
# Red   : FC0044444444

#
#   SJA1105Q-EVB: Port 0 internal to LPC
#
#            SJA1105Q
#
#       TJA1102     TJA1102 
#
#   Ports:  3   4   1   2
#
#   Addr:  14  15   8   9
#   LinkP: bl  re   si  ye

#   VLAN-TABLE

#   VLAN1: Port 1+2
#   VLAN2: Port 1+3
#   VLAN3: Port 2+3
#   VLAN4: Port 1+2+3


def main(args):
    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        if a.DevName == 'FC611':
            print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
            devTx = base.open_raw_tx(a.Inst)

            multiRawTx.add(a, devTx)

            devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

            multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()

    payload = 128
    data = ''
    for j in range(0, payload):
        data += '{0:02X}'.format(j)

    llcType = '{0:04X}'.format(payload + 7)
    llcHead = '1234'

    # Single-Tag: '81000015'
    # Double-Tag: '88A8000781000015'


    # Silver no VLAN
    frameListSilverNoVLAN = [
            ('FFFFFFFFFFFF', llcType, llcHead + 'FC00110001' + data),
            ('FC0022222222', llcType, llcHead + 'FC00110002' + data),
            ('FC0033333333', llcType, llcHead + 'FC00110003' + data),
            ('FC0044444444', llcType, llcHead + 'FC00110004' + data)
    ]

    frameListSilverDoubleVLAN = [
            ('FFFFFFFFFFFF', '88A8000781000015' + llcType, llcHead + 'FC00110001' + data)
    ]

    # correct VLAN 1 on Silver
    frameListSilverVLAN1 = [
            ('FFFFFFFFFFFF', '81000001' + llcType, llcHead + 'FC00110001' + data),
            ('FC0022222222', '81000001' + llcType, llcHead + 'FC00110002' + data),
            ('FC0033333333', '81000001' + llcType, llcHead + 'FC00110003' + data),
            ('FC0044444444', '81000001' + llcType, llcHead + 'FC00110004' + data)
    ]
    # wrong VLAN 5 on Silver
    frameListSilverVLAN5 = [
            ('FFFFFFFFFFFF', '810000050805', '010100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '810000050806', '010200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '810000050807', '010300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '810000050808', '010400010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]
    # no VLAN on Yellow
    frameListYellowNoVLAN = [
            ('FFFFFFFFFFFF', '0801', '010100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0011111111', '0802', '010100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0803', '010200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0804', '010300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]


    res = multiRawTx.send_raw_frames('silver', '', frameListSilverNoVLAN)

    res = multiRawTx.send_raw_frames('silver', '', frameListSilverDoubleVLAN)

    res = multiRawTx.send_raw_frames('silver', '', frameListSilverVLAN1)

    res = multiRawTx.send_raw_frames('silver', '', frameListSilverVLAN5)

    res = multiRawTx.send_raw_frames('yellow', '', frameListYellowNoVLAN)

    # wait short time until all frames are received 
    time.sleep(1)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)


    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))
        for rxFrame in rxFrames[dev]:
            ethRaw = RawRxFrame(1, rxFrame[1])
            print(ethRaw)

    time.sleep(0.8)

# End of Send/Receive reached.


# Validation will start here!

# Port1 (Silver) only send to Port4 (red)

    ExpectedResult = {'red': 12, 'silver': 0, 'yellow': 4, 'blue':0}
    ErrorCnt = 0
    for dev in ExpectedResult:
        try:
            numRxFrames = len(rxFrames[dev])
        except:
            numRxFrames = 0

        if numRxFrames != ExpectedResult[dev]:
            print('Validation Count Error in: {0} {1}!={2}'.format(dev, numRxFrames, ExpectedResult[dev]))
            ErrorCnt += 1
        

    if ErrorCnt > 0:
        print('?'*48)
        print('Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('Validation OK')
        print('-'*48)


# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)
    


if __name__ == "__main__":
    main(sys.argv)

