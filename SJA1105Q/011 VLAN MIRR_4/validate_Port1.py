import os, sys
import logging
import time
import binascii
import threading, queue

from broadway2 import Broadway, broadway_api, EthFrame, VLANType, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, EthRawFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

# Silver: FC0011111111
# Yellow: FC0022222222
# Orange: FC0033333333
# Red   : FC0044444444

#
#   SJA1105Q-EVB: Port 0 internal to LPC
#
#            SJA1105Q
#
#       TJA1102     TJA1102 
#
#   Ports:  3   4   1   2
#
#   Addr:  14  15   8   9
#   LinkP: or  re   si  ye

#   VLAN-TABLE

# Port1 VLAN-ID 2 (silver)
# Port2 VLAN-ID 3 (yellow)
# Port3 VLAN-ID 1 (orange)
# Port4 VLAN-ID 1 (red)


def main(args):
    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
        devTx = base.open_raw_tx(a.Inst)

        multiRawTx.add(a, devTx)

        devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

        multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()

    # no VLAN
    frameListNoVLAN_Silver = [
            ('FFFFFFFFFFFF', '0800', '010100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '0800', '010100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '010200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '010300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    # no VLAN
    frameListNoVLAN_Yellow = [
            ('FFFFFFFFFFFF', '0800', '020100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0011111111', '0800', '020100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '020200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '020300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    # no VLAN
    frameListNoVLAN_Orange = [
            ('FFFFFFFFFFFF', '0800', '030100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0011111111', '0800', '030100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '0800', '030200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '030300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    # no VLAN
    frameListNoVLAN_Red = [
            ('FFFFFFFFFFFF', '0800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0011111111', '0800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '0800', '040200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '040300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    # VLAN 1
    frameListVLAN_Red = [
            ('FFFFFFFFFFFF', '810000010800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0011111111', '810000010800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '810000010800', '040200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '810000010800', '040300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    # VLAN 3
    frameListVLAN3_Red = [
            ('FFFFFFFFFFFF', '810000030800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0011111111', '810000030800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '810000030800', '040200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '810000030800', '040300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]


    res = multiRawTx.send_raw_frames('silver', '', frameListNoVLAN_Silver)

    res = multiRawTx.send_raw_frames('yellow', '', frameListNoVLAN_Yellow)

    res = multiRawTx.send_raw_frames('orange', '', frameListNoVLAN_Orange)

    res = multiRawTx.send_raw_frames('red', '', frameListNoVLAN_Red)

    res = multiRawTx.send_raw_frames('red', '', frameListVLAN_Red)

    res = multiRawTx.send_raw_frames('red', '', frameListVLAN3_Red)

    # wait short time until all frames are received 
    time.sleep(1)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)


    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))
        for rxFrame in rxFrames[dev]:
            ethRaw = EthRawFrame(rxFrame[1])
            print(ethRaw)

    time.sleep(0.8)

# End of Send/Receive reached.


# Validation will start here!

    ExpectedResult = {'red': 12, 'silver': 24, 'yellow': 4, 'orange':8}
    ErrorCnt = 0
    for dev in rxFrames:
        if len(rxFrames[dev]) != ExpectedResult[dev]:
            print('Validation Error in: {0}'.format(dev))
            ErrorCnt += 1

    if ErrorCnt > 0:
        print('?'*48)
        print('VLAN Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('VLAN Validation OK')
        print('-'*48)


# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)
    


if __name__ == "__main__":
    main(sys.argv)

