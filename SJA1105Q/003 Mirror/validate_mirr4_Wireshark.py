import os, sys
import logging
import time
import threading, queue

from broadway2 import Broadway, broadway_api, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, EthRawFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

HWSetup = {'blue': 2, 'yellow': 0, 'silver': 1}



def main(args):

    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    rawList = base.get_raw_adapters()

    if len(rawList) != len(HWSetup):
        print('please verify hw-setup. Requires {0} x FC611 connected'.format(len(HWSetup)))
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        if a.UsbProductId == 0x8579:  # FC611
            print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
            devTx = base.open_raw_tx(a.Inst)

            multiRawTx.add(a, devTx)

            devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

            multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()
        
    payload = 128
    data = ''
    for j in range(0, payload):
        data += '{0:02X}'.format(j)

    llcType = '{0:04X}'.format(payload + 7)
    llcHead = '1234'


    frameListSilver = [
            ('FC0022222222', llcType, llcHead + 'FC00110001' + data),
            ('FC0033333333', llcType, llcHead + 'FC00110002' + data),
            ('FC0044444444', llcType, llcHead + 'FC00110003' + data)
    ]
    frameListYellow = [
            ('FC0011111111', llcType, llcHead + 'FC00220001' + data),
            ('FC0033333333', llcType, llcHead + 'FC00220002' + data),
            ('FC0044444444', llcType, llcHead + 'FC00220003' + data)
    ]

    frameListBlue = [
            ('FC0011111111', llcType, llcHead + 'FC00330001' + data),
            ('FC0022222222', llcType, llcHead + 'FC00330001' + data),
            ('FC0044444444', llcType, llcHead + 'FC00330001' + data)
    ]

    
    # Align this to frameList to get right expectations
    # -----------------------------------------------------------------
    ExpectedResult = {'silver': 3, 'yellow': 3, 'blue':3}
    # -----------------------------------------------------------------


# Here, we can now start to send data between ports

    res = multiRawTx.send_raw_frames('silver', '', frameListSilver)
    time.sleep(0.1)
    res = multiRawTx.send_raw_frames('yellow', '', frameListYellow)
    time.sleep(0.1)
    res = multiRawTx.send_raw_frames('blue', '', frameListBlue)
    
    
    time.sleep(0.5)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)

    for dev in rxFrames:
        for frame in rxFrames[dev]:
            ethRaw = EthRawFrame(frame[1])
            #print(ethRaw)
                    

    print('-'*48)
    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))

    time.sleep(0.5)

# End of Send/Receive reached.

    
    ErrorCnt = 0
    for dev in ExpectedResult:
        try:
            numRxFrames = len(rxFrames[dev])
        except:
            numRxFrames = 0

        if numRxFrames != ExpectedResult[dev]:
            print('Validation Count Error in: {0} {1}!={2}'.format(dev, numRxFrames, ExpectedResult[dev]))
            ErrorCnt += 1
        

    if ErrorCnt > 0:
        print('?'*48)
        print('Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('Validation OK')
        print('-'*48)


# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)


if __name__ == "__main__":
    main(sys.argv)

