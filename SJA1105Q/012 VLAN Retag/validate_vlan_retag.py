import os, sys
import logging
import time
import binascii
import threading, queue

from broadway2 import Broadway, broadway_api, EthFrame, VLANType, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

# Silver: FC0011111111
# Yellow: FC0022222222
# Orange: FC0033333333
# Red   : FC0044444444

#
#   SJA1105Q-EVB: Port 0 internal to LPC
#
#            SJA1105Q
#
#       TJA1102     TJA1102 
#
#   Ports:  3   4   1   2
#
#   Addr:  14  15   8   9
#   LinkP: or  re   si  ye

#   VLAN     21      19



def main(args):
    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        if a.DevName == 'FC611':
            print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
            devTx = base.open_raw_tx(a.Inst)

            multiRawTx.add(a, devTx)

            devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

            multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()

    # no VLAN
    frameListNoVLAN_Silver = [
            ('FC0022222222', '0800', '020100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '0800', '020200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '020300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    frameListNoVLAN_Orange = [
            ('FC0011111111', '0800', '010200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '0800', '010100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '0800', '010300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]



    # VLAN 19
    frameListVLAN19_Silver = [
            ('FC0022222222', '81000013' + '0800', '030100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0033333333', '81000013' + '0800', '030200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '81000013' + '0800', '030300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]

    # VLAN 21
    frameListVLAN21_Orange = [
            ('FC0011111111', '81000015' + '0800', '040100010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0022222222', '81000015' + '0800', '040200010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9'),
            ('FC0044444444', '81000015' + '0800', '040300010203040506070809BEEFA0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9A0A1A2A3A4A5A6A7A8A9')
    ]


    res = multiRawTx.send_raw_frames('silver', '', frameListNoVLAN_Silver)

    res = multiRawTx.send_raw_frames('orange', '', frameListNoVLAN_Orange)

    #res = multiRawTx.send_raw_frames('silver', '', frameListVLAN19_Silver)

    #res = multiRawTx.send_raw_frames('orange', '', frameListVLAN21_Orange)

    # wait short time until all frames are received 
    time.sleep(0.5)

    rxFrames = {}
    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)


    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))
        for rxFrame in rxFrames[dev]:
            ethRaw = RawRxFrame(1, rxFrame[1])
            print(ethRaw)

    time.sleep(0.8)

# End of Send/Receive reached.


# Validation will start here!

    ExpectedResult = {'red': 9, 'silver': 3, 'yellow': 9, 'orange':3}
    ErrorCnt = 0
    for dev in rxFrames:
        if len(rxFrames[dev]) != ExpectedResult[dev]:
            print('Validation Error in: {0}'.format(dev))
            ErrorCnt += 1

    if ErrorCnt > 0:
        print('Validation NOK')
    else:
        print('VLAN Validation OK')


# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)
    


if __name__ == "__main__":
    main(sys.argv)

