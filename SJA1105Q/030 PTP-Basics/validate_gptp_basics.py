import os, sys
import logging
import time
import threading, queue

from broadway2 import Broadway, broadway_api, MultiRawSender, MultiRawReceiverThread, RawRxHeaderTypes, RawRxFrame

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)


# HW-Setup for FC611 and SJA1105Q

HWSetup = {'red': 3, 'orange': 2, 'yellow': 0, 'silver': 1}

MACAddress = {'silver': 0xFC0011111111, 'yellow': 0xFC0022222222, 'orange': 0xFC0033333333, 'red': 0xFC0044444444}

def main(args):

    # config RawReceiver requires q and stop event to communicate with Rx-Thread
    stop = threading.Event()
    q = queue.Queue()

    multiRawRx = MultiRawReceiverThread(q, stop)

    multiRawTx = MultiRawSender()

    # create adapter singleton
    base = Broadway()

    if base.Count < 2:
        print('requires minimum of 2xFC611')
        sys.exit(-1)

    print('Validation started: {0} Adapters found'.format(base.Count))

    # configure multi-raw-sender and receiver
    for a in base.enumerate():
        print('found {0}/{1} on {2} and added to list.'.format(a.FriendlyName, a.MACAddress, a.Inst))
        devTx = base.open_raw_tx(a.Inst)

        multiRawTx.add(a, devTx)

        devRx = base.open_raw_rx(a.Inst, RawRxHeaderTypes.HeaderType2)

        multiRawRx.add(a, devRx)

    
    multiRawRx.rx_enable()
    multiRawRx.start()

    payload = 128
    data = ''
    for j in range(0, payload):
        data += '{0:02X}'.format(j)

    llcType = '{0:04X}'.format(payload + 7)
    llcHead = '1234'


    frameListSilver = [
            ('FFFFFFFFFFFF', llcType, llcHead + 'FC00110001' + data),
            ('0180C200000E', '88F7', '12020036000000080000000000000000000000004a5065fffe726e790001df7505000000000000000000000000000000000000000000')
    ]
    
    # Align this to frameList to get right expectations
    # -----------------------------------------------------------------
    ExpectedResult = {'red': 1, 'silver': 0, 'yellow': 1, 'orange':1}
    # -----------------------------------------------------------------


# Here, we can now start to send data between ports

    res = multiRawTx.send_raw_frames('silver', '', frameListSilver)
    
    time.sleep(1)

    rxFrames = {}

    while not q.empty():
        rxData = q.get(False, 500)
        try:
            rxFrames[rxData[0]].append(rxData)
        except:
            rxFrames[rxData[0]] = []
            rxFrames[rxData[0]].append(rxData)

    for dev in rxFrames:
        for rxFrame in rxFrames[dev]:
            ethRaw = RawRxFrame(1, rxFrame[1])
                    

    print('-'*48)
    for dev in rxFrames:
        print('Received Frames on: {0} = {1}'.format(dev, len(rxFrames[dev])))

    time.sleep(0.5)

# End of Send/Receive reached.

    ErrorCnt = 0
    for dev in ExpectedResult:
        try:
            numRxFrames = len(rxFrames[dev])
        except:
            numRxFrames = 0

        if numRxFrames != ExpectedResult[dev]:
            print('Validation Error in: {0} {1}!={2}'.format(dev, numRxFrames, ExpectedResult[dev]))
            ErrorCnt += 1
        


    if ErrorCnt > 0:
        print('?'*48)
        print('Validation NOK')
        print('?'*48)
    else:
        print('-'*48)
        print('Validation OK')
        print('-'*48)




# Stop Receiver and close all devices

    multiRawRx.rx_disable()
    stop.set()

    # close all devTx devices
    for dev in multiRawTx.enumerate():
        base.close(dev)

    # close all devRx devices
    for dev in multiRawRx.enumerate():
        base.close(dev)


if __name__ == "__main__":
    main(sys.argv)

