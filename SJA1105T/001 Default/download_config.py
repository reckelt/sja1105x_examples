# Python3.x
import os, sys
import time

from sja1105_platform import SJA1105Platform, SJA1105PlatformDefs, PlatformHelper
from sja1105 import SJA1105CGU, SJA1105ACU

HexFileT = 'SJA1105T_Base.hex'
LocalPath = os.path.split(__file__)[0]


CGUSettingsT = [
    # initialize PLL1 50MHz
    {'Reg': SJA1105CGU.PLL_1_C, 'Value': 0x0A010141},
    {'Reg': SJA1105CGU.PLL_1_C, 'Value': 0x0A010940},

    # port 0: RMII (MAC-MODE)
    {'Reg': SJA1105CGU.RMII_REF_CLK_0, 'Value':0x00000800},
    {'Reg': SJA1105CGU.EXT_TX_CLK_0, 'Value': 0x0E000800},
    
    # port 1: RMII (MAC-MODE)
    {'Reg': SJA1105CGU.RMII_REF_CLK_1, 'Value': 0x02000800},
    {'Reg': SJA1105CGU.EXT_TX_CLK_1, 'Value': 0x0E000800},

    # port 2: RMII (MAC-MODE)
    {'Reg': SJA1105CGU.RMII_REF_CLK_2, 'Value': 0x04000800},
    {'Reg': SJA1105CGU.EXT_TX_CLK_2, 'Value': 0x0E000800},

    # port 3: RMII (MAC-MODE)
    {'Reg': SJA1105CGU.RMII_REF_CLK_3, 'Value': 0x06000800},
    {'Reg': SJA1105CGU.EXT_TX_CLK_3, 'Value': 0x0E000800},

    # port 4: RMII_LPC1788 (PHY-MODE, external REFCLK)
    {'Reg': SJA1105CGU.IDIV_4_C, 'Value': 0x0A000001},
    {'Reg': SJA1105CGU.RMII_REF_CLK_4, 'Value': 0x08000800}
]


# use default settings 
ACUSettings = [
    {'Reg': SJA1105ACU.CFG_PAD_MII0_TX, 'Value': 0x0A0A0A0A},    # low noise/medium speed
    {'Reg': SJA1105ACU.CFG_PAD_MII1_TX, 'Value': 0x0A0A0A0A},    # low noise/medium speed
    {'Reg': SJA1105ACU.CFG_PAD_MII2_TX, 'Value': 0x0A0A0A0A},    # low noise/medium speed
    {'Reg': SJA1105ACU.CFG_PAD_MII3_TX, 'Value': 0x0A0A0A0A},    # low noise/medium speed
    {'Reg': SJA1105ACU.CFG_PAD_MII4_TX, 'Value': 0x0A0A0A0A}    # low noise/medium speed
]

PhySettings = {0: 'Master',
               1: 'Master',
               2: 'Master',
               3: 'Master'
               }



hlp = PlatformHelper()
hlp.detect()

if len(hlp.platformPorts) != 1:
    print('make sure to connect only one EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0) # we only support 1 connected board here

try:
    platform = SJA1105Platform(port)    # w
except Exception as e:
    print(e)
    sys.exit(-1)

print('START')
currentPlatformVersion = platform.get_version()

if currentPlatformVersion == SJA1105PlatformDefs.SJA1105T:
    HexFile = os.path.join(LocalPath, HexFileT)
else:
    print('Unknown platform!')
    sys.exit(-2)


print('HexFile:         {0}'.format(HexFile))
print('uC SW version:   {0:08X}'.format(currentPlatformVersion))    # read the microcontroller software version
print('DeviceId:        {0:08X}'.format(platform.spi_read(0)))      # read the deviceId
print('Status register: {0:08X}'.format(platform.spi_read(1)))      # read the status register


print('-'*20)

print('reset')
platform.reset()

print('-'*20)

print('Setup Phys')

for phy in PhySettings:
    phyAddr = platform.get_phy_addr(phy)
    print(phyAddr)
    reg23 = platform.smi_read(phyAddr, 23)
    reg23 = reg23 | 4   # enable Config
    platform.smi_write(phyAddr, 23, reg23)

    reg23 = reg23 & ~0x8000
    platform.smi_write(phyAddr, 23, reg23)

    reg18 = platform.smi_read(phyAddr, 18)
    reg18 = reg18 | 0x8000
    platform.smi_write(phyAddr, 18, reg18)

    reg23 = reg23 | 0x8000
    platform.smi_write(phyAddr, 23, reg23)

print('Setup CGU')
for cguReg in CGUSettings:
        platform.spi_write(cguReg['Reg'], cguReg['Value'])

print('Setup ACU')
for acuReg in ACUSettings:
        platform.spi_write(acuReg['Reg'], acuReg['Value'])

print('download {0}'.format(HexFile))

statusBefore = platform.spi_read(1)
print('Status register before: {0:08x}'.format(statusBefore))
# Configure Switch using binary-hex file
platform.spi_config(HexFile)

statusAfter = platform.spi_read(1)
print('Status register after:  {0:08X}'.format(statusAfter))


print('-'*20)


hlp.close(port)

