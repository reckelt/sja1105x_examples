"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys
import prettytable

from sja1105_platform import PlatformHelper, SJA1105Platform
    
#Values to remain unchanged
VALID               = 1     ##Triggers dynamic change
RDWRSET             = 0     ##0 - > Read 1 -> Write
MGMTROUTE           = 1     ##0 -> L2 entry 1 -> Management entry
HOSTCMD             = 2     ## 2 -> read
L2_ADDRESS          = 0x24  ##Start address of the L2 address lookup table
L2_CONTROL_ADDRESS  = 0x29  ##L2 address lookup table control register
TABLE_SIZE          = 4     ##Table size
ADDRESS_RANGE       = 5     ##Number of addresses to be read
NUM_PORTS           = 5     ##Number of ports
MAC_LENGTH          = 6     ##Length of MAC address


##Control Data
controlData = ((VALID << 31) + (RDWRSET << 30) + (MGMTROUTE << 26) + (HOSTCMD << 23))

def mac_to_str(mac):
    return ':'.join(['%02X' % ((MACADDR >> (8 * (6-1-k))) & 0xFF) for k in range(6)])   

def progress(status, total):
    bar_len = 60
    filled_len = int(round(bar_len * status / float(total)))
    padding_len = bar_len - filled_len

    percents = round(100.0 * status / float(total), 1)
    bar = '=' * filled_len + '-' * padding_len

    sys.stdout.write('[{0}] {1}%\r'.format(bar, percents))
    sys.stdout.flush()  

# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()



print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)    
 
try:
    header = ("INDEX", "DESTPORT", "MACADDR", "TAKETS", "TSREG", "STATUS")
    table = prettytable.PrettyTable(header)    
    
    ##Write to control register
    for i in range (TABLE_SIZE):
        platform.spi_write(L2_ADDRESS, (i << 6))                   ##Write index to be read
        platform.spi_write(L2_CONTROL_ADDRESS, controlData)         ##Control data write
        
        validEntry = platform.spi_read(L2_CONTROL_ADDRESS)          ##Check for valid entry
        
        progress (i, TABLE_SIZE) 
      

        if ((validEntry & 0x04000000) == 0x04000000):            
            readData = [platform.spi_read(L2_ADDRESS + j) for j in range (ADDRESS_RANGE)]
                
            INDEX       =  ((readData[0] >> ( 6 % 32)) & 0x3FF)
            MGMTVALID   =  ((readData[0] >> (16 % 32)) &     1)
            DESTPORT    =  ((readData[0] >> (17 % 32)) & 0x01f)
            MACADDR     = (((readData[0] >> 22) & 0x000003ff) <<  0) + \
                          (((readData[1] >>  0) & 0xffffffff) << 10) + \
                          (((readData[2] >>  0) & 0x0000003f) << 42)
            TAKETS      =  ((readData[2] >> (70 % 32)) &    1)
            TSREG       =  ((readData[2] >> (71 % 32)) &    1)


            entry = [str(INDEX), '{0:05b}'.format(DESTPORT), mac_to_str(MACADDR), str(TAKETS), str(TSREG), str(MGMTVALID)]
            table.add_row(entry)
           
    print()
    print('Management Lookup Table Read Complete')
    print(table.get_string())
         
except Exception as e: # catch all exceptions
    print(e)

