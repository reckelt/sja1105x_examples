"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys

from sja1105_platform import PlatformHelper, SJA1105Platform


# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()

print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)    

##Values to remain unchanged
BASE_ADDRESS        = 0x1400        ##(1024) Port 0
OFFSET_PORT_ADDRESS = 0x18         ##Offset address between sucessive ports
NUMBER_OF_COUNTERS  = 4            ##Number of counters to be read
PORTS               = [0,1,2,3,4]  ##Port numbers

##Local variables

RegisterMap = {
    'N_DROPS_NO_LEARN': 0x16,
    'N_DROPS_EMPTY_ROUTE': 0x15,
    'N_DROPS_ILL_DTAG': 0x14,
    'N_DROPS_DTAG': 0x13,
    'N_DROPS_SOTAG': 0x12,
    'N_DROPS_SITAG': 0x11,
    'N_DROPS_UTAG' : 0x10,
    'N_TX_MCAST': 0x09,
    'N_TX_BCAST': 0x08,
    'N_RX_MCAST': 0x01,
    'N_RX_BCAST': 0x00
}

header = '{:>20} {:<8} {:<8} {:<8} {:<8} {:<8}'.format("REGISTER","Port0","Port1","Port2","Port3","Port4")


counterData = [0]*len(RegisterMap)*len(PORTS)
##Read diagnostic counters      
for port in PORTS:
    count = 0
    for reg in RegisterMap:
        addr   = BASE_ADDRESS + (port * OFFSET_PORT_ADDRESS) + RegisterMap[reg]
   
        regValue = platform.spi_read(addr)

        #print('Reg: {0:04X} = {1:<8}'.format(addr, regValue))
        
        counterData[count*len(PORTS) + port] = regValue
        count += 1

print(header)
print('='*len(header))

count  = 0
for reg in RegisterMap:
    output = '{0:>20} '.format(reg)
    for port in range(0, len(PORTS)):
        output += '{0:<8} '.format(counterData[count*len(PORTS)+port])
    count = count + 1
    print(output)

print('-'*len(header))
print('Diagnostic Registers Read Sucessfully')
                   
