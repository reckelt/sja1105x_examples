"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys
sys.path.append('..\modules')

import uart as uart_platform
from modules import prettytable

try:
    platform = uart_platform.UartPlatform("auto")    # the integer between the brackets specify which board to use. This value corresponds to encoder X10 on the board
except Exception as e:
    print e
    exit(0)

def mac_to_str(mac):
    return ':'.join(['%02X' % ((MACADDR >> (8 * (6-1-k))) & 0xFF) for k in range(6)])

##Configurable values
DESTPORTS           = 0x03                      ##(_ _ _ P4 P3 P2 P1 P0 ) Destination Ports.
MACADDR             = 0x0180C200000E            ##MAC address of the management frame.
INDEX               = 1                         ##Physical memory address
TAKETS              = 1                         ##Enable egress timestamp 0 -> Disable 1 -> Enable
TSREG               = 1                         ##Register for TS 0 -> Reg0 1 -> Reg1

##Values to remain unchanged
MGMTVALID           = 1                         ##Flag must be asserted for programming and will be invalidated when single frame received
VALID               = 1                         ##Triggers dynamic change                
RDWRSET             = 1                         ##Read or write access
VALIDENT            = 1                         ##Validate entry
MGMTROUTE           = 1                         ##Managment route indication
HOSTCMD             = 3                         ##2 -> read; 3 -> write
L2_ADDRESS          = 0x24                      ##Start address of the L2 address lookup table
L2_CONTROL_ADDRESS  = 0x29                      ##L2 address lookup table control register
ADDRESS_RANGE       = 3                         ##Number of addresses to be read
NUM_PORTS           = 5                         ##Number of ports
MAC_LENGTH          = 6                         ##Length of MAC address



l2Entry = []
##L2 Entry
tmp  = 0
tmp |= ((INDEX & 0x03ff)               << ( 6 % 32))
tmp |= ((MGMTVALID & 1)                << (16 % 32))
tmp |= ((DESTPORTS & 0x1f)             << (17 % 32))
tmp |= ((MACADDR & 0x3ff)              << (22 % 32))
l2Entry.append(tmp) # 0
tmp  = 0
tmp |= ((MACADDR >> 10) & 0xffffffff)
l2Entry.append(tmp) # 1
tmp  = 0
tmp |= (((MACADDR >> 42) & 0x3f)       << (64 % 32))
tmp |= ((TAKETS & 1)                   << (70 % 32))
tmp |= ((TSREG & 1)                    << (71 % 32))
l2Entry.append(tmp) # 2

##Control register
controlL2Entry = ((VALID << 31) + (RDWRSET << 30) + (VALIDENT << 27) + (MGMTROUTE << 26) + (HOSTCMD << 23))

   
try:

    ##Writing to the lookup table
    for i in range (len(l2Entry)):
        platform.spi_write(L2_ADDRESS + i, l2Entry[i])

    ##Write Control
    platform.spi_write(L2_CONTROL_ADDRESS, controlL2Entry)
        
    table = prettytable.PrettyTable(("INDEX", "DESTPORT", "MACADDR", "TAKETS", "TSREG"))    
    entry = (str(INDEX), '{0:05b}'.format(DESTPORTS), mac_to_str(MACADDR), str(TAKETS), str(TSREG))
    table.add_row(entry)
    print table.get_string()
   
    print "Management Route Entry Written Sucessfully"
   
   
except Exception as e: # catch all exceptions
    print e