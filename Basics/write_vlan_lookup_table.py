"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys
sys.path.append('..\modules')

import uart as uart_platform
from modules import prettytable

try:
    platform = uart_platform.UartPlatform("auto")    # the integer between the brackets specify which board to use. This value corresponds to encoder X10 on the board
except Exception as e:
    print e
    exit(0)


##VLAN Configuration
VLANID              = 25    ##VLANID
VMEMB_PORT          = 0x07  ##Member ports       (_ _ _ P4 P3 P2 P1 P0)
VLAN_BC             = 0x07  ##Broadcast domain   (_ _ _ P4 P3 P2 P1 P0)
TAG_PORT            = 0x07  ##Tagged ports       (_ _ _ P4 P3 P2 P1 P0)

##Mirroring configuration.
##Note:On enabling mirroring for a port the traffic in/out of the port is forwarded to the mirror port as per MIRR_PORT
VING_MIRR           = 0x18  ##Ingress mirroring  (_ _ _ P4 P3 P2 P1 P0)
VEGR_MIRR           = 0x10  ##Egress mirroring   (_ _ _ P4 P3 P2 P1 P0)

##Values to remain unchanged
VALID               = 1     ## 1 -> Update requested on Write;
RDWRSET             = 1     ## 0 -> Read; 1 -> Write
VALIDENT            = 1     ## Validate entry
VLAN_ADDRESS        = 0x2d  ## Start address of the VLAN lookup table
VLAN_ADDRESS_CTL    = 0x30  ## Address of VLAN lookup table control registers
ADDRESS_RANGE       = 2     ## Number of addresses to be read
NUM_PORTS           = 5     ## Number of ports



vlanEntry = []
##L2 Entry
vlanEntry.append((VLANID & 0x1F) << 27)
vlanEntry.append((VLANID >> 5) + (TAG_PORT << (39 % 32)) + (VLAN_BC << (44 % 32)) + (VMEMB_PORT << (49 % 32)) + (VEGR_MIRR << (54 % 32)) + (VING_MIRR << (59 % 32)))

##Control register
controlVlanEntry = ((VALID << 31) + (RDWRSET << 30) + (VALIDENT << 27))

try:
    header = ("VLANID", "MEM-PORTS", "BC-DOMAIN", "TAGGED-PORTS", "MIRR-IN", "MIRR-EGR")
    table = prettytable.PrettyTable(header)

    ##Writing to the lookup table
    for i in range (len(vlanEntry)):
        platform.spi_write(VLAN_ADDRESS + i, vlanEntry[i])

    ##Write Control
    platform.spi_write(VLAN_ADDRESS_CTL, controlVlanEntry)

    def pfrmt(x): return '{0:05b}'.format(x)

    entry = [str(VLANID), pfrmt(VMEMB_PORT), pfrmt(VLAN_BC), pfrmt(TAG_PORT), pfrmt(VING_MIRR), pfrmt(VEGR_MIRR)]
    table.add_row(entry)
    print table.get_string()

    print "VLAN Lookup Table Entry Written"


except Exception as e: # catch all exceptions
    print e