# Verify if all required packages are installed
import importlib

try:
    from pip import main as pipmain
except:
    from pip._internal import main as pipmain



listOfPackages = ['serial', 'intelhex', 'prettytable']
listOfToInstall = []

for p in listOfPackages:
    try:
        module = importlib.import_module(p)
    except:
        print('package missing: {0}'.format(p))
        listOfToInstall.append(p)

for p in listOfToInstall:
    print('need to install: {0}'.format(p))
