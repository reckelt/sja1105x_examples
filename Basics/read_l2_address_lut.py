
import sys
import prettytable

from sja1105_platform import PlatformHelper, SJA1105Platform

##Values to remain unchanged
VALID               = 1               ## Triggers dynamic change
RDWRSET             = 0               ## 0 -> Read; 1 -> Write
HOSTCMD             = 2               ## 2 -> read
MGMTROUTE           = 0               ## 0 -> L2 entry 1 -> Management entry
L2_ADDRESS          = 0x24            ## Start address of the L2 address lookup table
L2_CONTROL_ADDRESS  = 0x29            ## L2 address lookup table control register
TABLE_SIZE          = 1024            ## Table size 
ADDRESS_RANGE       = 5               ## Number of addresses to be read
NUM_PORTS           = 5               ## Number of ports

def mac_to_str(macAddr):
    return '{0:04X}.{1:04X}.{2:04X}'.format((macAddr >> 32) & 0xFFFF, (macAddr >> 16) & 0xFFFF, macAddr & 0xFFFF)

def progress(status, total):
    bar_len = 60
    filled_len = int(round(bar_len * status / float(total)))
    padding_len = bar_len - filled_len

    percents = round(100.0 * status / float(total), 1)
    bar = '=' * filled_len + '-' * padding_len

    sys.stdout.write('[{0}] {1}%\r'.format(bar, percents))
    sys.stdout.flush()  

# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()



print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)    
try:
    header = ("INDEX", "DESTPORT", "MACADDR", "VLANID", "ENFPORT", "IOTAG", "LOCKEDS")
    table = prettytable.PrettyTable(header)
    
    ##Write to control register
    for i in range (TABLE_SIZE):
        ##Write index to be read
        platform.spi_write(L2_ADDRESS,         ((i & 0x3ff) << 6))
        
        ##Control data write - set flags for read
        platform.spi_write(L2_CONTROL_ADDRESS, ((VALID << 31) + (RDWRSET << 30) + (HOSTCMD << 23)))
        
        ##Check for valid entry
        validEntry = platform.spi_read(L2_CONTROL_ADDRESS)
        progress (i, TABLE_SIZE) 
        
        if ((validEntry & (1<<27)) == (1<<27)):
        
            readData = [platform.spi_read(L2_ADDRESS + j) for j in range (ADDRESS_RANGE)]            
            
            LOCKEDS     = ((validEntry  >> 28) & 1)
                                   
            INDEX       =  ((readData[0] >> ( 6 % 32)) & 0x3FF)
            ENFPORT     =  ((readData[0] >> (16 % 32)) &     1)
            DESTPORT    =  ((readData[0] >> (17 % 32)) & 0x01f)
            MACADDR     = (((readData[0] >> 22) & 0x000003ff) <<  0) + \
                          (((readData[1] >>  0) & 0xffffffff) << 10) + \
                          (((readData[2] >>  0) & 0x0000003f) << 42)
            VLANID      =  ((readData[2] >> (70 % 32)) & 0xfff)
            IOTAG       =  ((readData[2] >> (82 % 32)) &    1)
            
                
            entry = (str(INDEX), '{0:05b}'.format(DESTPORT), mac_to_str(MACADDR), str(VLANID), str(ENFPORT), str(IOTAG), str(LOCKEDS))
            table.add_row(entry)
                           
    print()
    print('L2 Address Lookup Table Read Complete')

    print(table.get_string())
         
except Exception as e: # catch all exceptions
    print(e)
