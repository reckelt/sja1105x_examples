import sys
import time
sys.path.append("..\\modules")

import uart as uart_platform
from modules import prettytable
from prettytable import PrettyTable
from struct import *


counter_p1=0
new_counter_p1=0
counter_p2=0
new_counter_p2=0
counter_p3=0
new_counter_p3=0
counter_p4=0
new_counter_p4=0
vlanid=''
vlanid2=''
vlanid3=''
vlanid4=''

x= PrettyTable()
x.field_names = ["Port", "WrongVLANID","ErrorCounter"]
try:
    platform = uart_platform.UartPlatform("COM3")    # the integer between the brackets specify which board to use. This value corresponds to encoder X10 on the board
except Exception as e:
    print (e)
    exit(0)
    
    
############################################################


while True:
	try:
		reg=  "0x%08X" %( ( platform.spi_read( 7 ) ))
		counter_p1=new_counter_p1
		counter_p2=new_counter_p2
		counter_p3=new_counter_p3
	
		if reg != "0x00000000":
			port= reg[-3]
		
		
		
			#print vlanid
			if port =='1':
				vlanid= reg[:-4]
				vlanid=vlanid.lstrip('0x')
				new_counter_p1=counter_p1+1
			
			
			if port =='2':
				vlanid2= reg[:-4]
				new_counter_p2=counter_p2+1
				vlanid2=vlanid2.lstrip('0x')
			
			if port =='3':
				vlanid3= reg[:-4]
				new_counter_p3=counter_p3+1
				vlanid3=vlanid3.lstrip('0x')
			
			
			if port =='4':
				vlanid4= reg[:-4]
				new_counter_p4=counter_p4+1
				vlanid4=vlanid4.lstrip('0x')
			
		
			x.add_row([1,vlanid,new_counter_p1])
			x.add_row([2,vlanid2,new_counter_p2])
			x.add_row([3,vlanid3,new_counter_p3])
			x.add_row([4,vlanid4,new_counter_p4])
			print (x)
			print('Dropped frame on port: ' + port)
			print ('Please check VLAN LookupTable')
			x.clear_rows()
			
	except KeyboardInterrupt:
			x.add_row([1,vlanid,new_counter_p1])
			x.add_row([2,vlanid2,new_counter_p2])
			x.add_row([3,vlanid3,new_counter_p3])
			x.add_row([4,vlanid4,new_counter_p4])
			with open('file.txt','a') as w:
				w.write(str(x))
				break

			 
	