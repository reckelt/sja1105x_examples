import sys
import time
from datetime import datetime
from sja1105_platform import PlatformHelper, SJA1105Platform
from broadway2 import Broadway, get_phy_access


ConnectionSetup = [
    {'FC611': 'silver', 'TJA1102': 8},
    {'FC611': 'yellow', 'TJA1102': 9},
    {'FC611': 'blue', 'TJA1102': 14},
    {'FC611': 'red', 'TJA1102': 15}
]


def print_switch_state(platform):
    print('Platform State: {0}'.format(datetime.now()))
    print('-'*20)
    for idx in range(0, platform.NumPhyPorts):
        phyAddr = platform.get_phy_addr(idx)
        phyState = (platform.smi_read(phyAddr, 23) & 0x8000) == 0x8000
        print('{0:2} {1}'.format(phyAddr, phyState))


def get_switch_phy_state(platform):
    res = {}
    for idx in range(0, platform.NumPhyPorts):
        phyAddr = platform.get_phy_addr(idx)
        phyState = (platform.smi_read(phyAddr, 23) & 0x8000) == 0x8000
        res[phyAddr] = phyState
    return res

# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()

# FC611 helper to detect USB Adapters
base = Broadway()


print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)


platform = SJA1105Platform(port)


print_switch_state(platform)

print()

print('USB Adapters')
print('-'*20)

fc611List = {}
for adapter in base.enumerate():
    print('Adapter: {0:8} / {1}'.format(adapter.FriendlyName, adapter.MACAddress))
    if adapter.PhyType == 'NXP TJA1100':
        fc611List[adapter.FriendlyName] = adapter

initialState = get_switch_phy_state(platform)
# disable Link-Control for all connected adapters
for fc611 in fc611List:
    fca = fc611List[fc611]
    dev = base.open_control(fca.Inst)
    fca.connect(dev)
    fca.LinkState = False
    base.close(dev)

offState = get_switch_phy_state(platform)
time.sleep(1)

Connections = {}
for fc611 in fc611List:
    print('Check state: {0}...'.format(fc611))
    fca = fc611List[fc611]
    dev = base.open_control(fca.Inst)
    fca.connect(dev)
    fca.LinkState = True
    time.sleep(3)   # make sure, that SJA1105Q-Platform gets active phy
    onState = get_switch_phy_state(platform)
    for s in onState:
        if onState[s] == True:
            Connections[fc611] = {'Inst': fc611List[fc611].Inst, 'TJA1102': s}
    fca.LinkState = False
    base.close(dev)

# verify finished, now enable all Link-Control

for fc611 in fc611List:
    fca = fc611List[fc611]
    dev = base.open_control(fca.Inst)
    fca.connect(dev)
    fca.LinkState = True

    base.close(dev)

# Verify connections with target

errCnt = 0
for c in ConnectionSetup:
    try:
        devName = c['FC611']
        #print(devName, Connections[devName])
        if c['TJA1102'] != Connections[devName]['TJA1102']:
            errCnt += 100
    except:
        errCnt += 1

print('--------------')

if errCnt > 0:
    print('Validate Status: ERROR ({0})'.format(errCnt))
    print(ConnectionSetup)
    print(Connections)
else:
    print('Validate Status: OK')

print('--------------')
