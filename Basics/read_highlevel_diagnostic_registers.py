"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys

from sja1105_platform import PlatformHelper, SJA1105Platform


# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()

print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)    

##Values to remain unchanged
BASE_ADDRESS        = 0x400        ##(1024) Port 0
OFFSET_PORT_ADDRESS = 0x10         ##Offset address between sucessive ports
NUMBER_OF_COUNTERS  = 4            ##Number of counters to be read
PORTS               = [0,1,2,3,4]  ##Port numbers

##Local variables

outputformat = '{:>12}   {:<8} {:<8} {:<8} {:<8} {:<8}'
header = outputformat.format("REGISTER","Port0","Port1","Port2","Port3","Port4")
diagReg = ['N_TXBYTE','N_TXFRM','N_RXBYTE','N_RXFRM', ]

counterData = [0]*len(diagReg)*len(PORTS)
##Read diagnostic counters      
for port in PORTS:
    count = 0
    for relAddr in range (0,NUMBER_OF_COUNTERS):
        addr   = BASE_ADDRESS + (port * OFFSET_PORT_ADDRESS) + relAddr * 2
        addrSh = BASE_ADDRESS + (port * OFFSET_PORT_ADDRESS) + relAddr * 2 + 1
   
        reg   = platform.spi_read(addr)
        regSh = platform.spi_read(addrSh)

        tmp = reg + (regSh << 32)

        #print('Reg: {0:04X} = {1:<8}'.format(addr, tmp))
        
        counterData[count*len(PORTS) + port] = tmp
        count += 1

print(header)

count  = 0
for i in range(0,len(diagReg)):
    output = '{0:>12}'.format(diagReg[i])
    for port in range(0, len(PORTS)):
        output += '{0:8} '.format(counterData[count*len(PORTS)+port])
    count = count + 1
    print(output)

# N_POLERR at register offset 8h
for port in PORTS:
    addr = BASE_ADDRESS + (port * OFFSET_PORT_ADDRESS) + 0x8
    reg   = platform.spi_read(addr)
    
    print('N_POLERR: {0}: {1}'.format(port, reg))

print('Diagnostic Registers Read Sucessfully')
                   
