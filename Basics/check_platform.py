# Verify platform access sja1105_platform (using pyserial)


from sja1105_platform import PlatformHelper, SJA1105Platform

hlp = PlatformHelper()
hlp.detect()

print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

for p in hlp.platformPortMap:

    print('Platform: {0} on {1}'.format(p, hlp.platformPortMap[p]))
    print('-'*40)

    port = hlp.open(hlp.platformPortMap[p]) 

    platform = SJA1105Platform(port)


    print('Version: {0:08X} Name:{1} NumPhyPorts: {2}'.format(platform.platformVersion, platform.PlatformName, platform.NumPhyPorts))

    for spiReg in range(0,4):
        regValue = platform.spi_read(spiReg)
        print('SPI[{0:02X}] = {1:08X}'.format(spiReg, regValue))

    smiRegs = [1, 2, 3]
    for idx in range(0, platform.NumPhyPorts):
        phyAddr = platform.get_phy_addr(idx)
        for smiReg in smiRegs:
            regValue = platform.smi_read(phyAddr, smiReg)
            print('SMI[{0:02}][{1:02X}] = {2:08X}'.format(idx, smiReg, regValue))

    hlp.close(port)
