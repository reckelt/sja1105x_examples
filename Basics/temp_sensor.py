import time
from sja1105_platform import PlatformHelper, SJA1105Platform
from sja1105 import SJA1105PMisc


hlp = PlatformHelper()
hlp.detect()

print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound == 1:
    port = hlp.open_by_index(0)

    platform = SJA1105Platform(port)

    for level in range(0x27, 0, -1):
        config = platform.spi_read(SJA1105PMisc.TS_CONFIG)
        newConfig = (config & ~0x3F) | level
        platform.spi_write(SJA1105PMisc.TS_CONFIG, newConfig)
        time.sleep(0.1)
        regValue = platform.spi_read(SJA1105PMisc.TS_STATUS)

        print('TS_Config: {0} TS_STATUS: {1}'.format(level, regValue))
        time.sleep(0.1)

    hlp.close(port)
