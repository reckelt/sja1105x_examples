"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys
import prettytable

from sja1105_platform import PlatformHelper, SJA1105Platform
    
##VLAN Configuration
VLANID              = 4     ##VLANID
VMEMB_PORT          = 0x07  ##Member ports       (_ _ _ P4 P3 P2 P1 P0)
VLAN_BC             = 0x07  ##Broadcast domain   (_ _ _ P4 P3 P2 P1 P0)
TAG_PORT            = 0x07  ##Tagged ports       (_ _ _ P4 P3 P2 P1 P0)

##Values to remain unchanged
VALID               = 1     ##1 -> Update requested on Write; 
RDWRSET             = 0     ##0 -> Read; 1 -> Write
VLAN_ADDRESS        = 0x2d  ##Start address of the VLAN lookup table
VLAN_ADDRESS_CTL    = 0x30  ##Address of VLAN lookup table control registers
TABLE_SIZE          = 4096  ##Table size
ADDRESS_RANGE       = 2     ##Number of addresses to be read
NUM_PORTS           = 5     ##Number of ports

def progress(status, total):
    bar_len = 60
    filled_len = int(round(bar_len * status / float(total)))
    padding_len = bar_len - filled_len

    percents = round(100.0 * status / float(total), 1)
    bar = '=' * filled_len + '-' * padding_len

    sys.stdout.write('[{0}] {1}%\r'.format(bar, percents))
    sys.stdout.flush()  



# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()

print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)    

try:
    
    header = ("VLANID", "MEM-PORTS", "BC-DOMAIN", "TAGGED-PORTS", "MIRR-IN", "MIRR-EGR")
    table = prettytable.PrettyTable(header)    

    for i in range (TABLE_SIZE):
        ##Write vlanid to be read
        platform.spi_write(VLAN_ADDRESS,       ((i &  0x1f) << 27))
        platform.spi_write(VLAN_ADDRESS+1,     ((i >> 5) & 0x7f))
        
        ##Control data write - set flags for read
        platform.spi_write(VLAN_ADDRESS_CTL, ((VALID << 31) + (RDWRSET << 30)))
        
        ##Check for valid entry
        validEntry = platform.spi_read(VLAN_ADDRESS_CTL)

        progress (i, TABLE_SIZE) 
        
        if ((validEntry & (1<<27)) == (1<<27)):

            readData = [platform.spi_read(VLAN_ADDRESS + j) for j in range (ADDRESS_RANGE)]
        
            VALIDENT = ((validEntry >> 27) & 1)

            VLANID      = (((readData[0] >> 27) & 0x0000001f) <<  0) + \
                          (((readData[1] >>  0) & 0x0000007f) <<  5)
            TAG_PORT    =  ((readData[1] >> (39 % 32)) &  0x1f)
            VLAN_BC     =  ((readData[1] >> (44 % 32)) &  0x1f) 
            VMEMB_PORT  =  ((readData[1] >> (49 % 32)) &  0x1f)
            VEGR_MIRR   =  ((readData[1] >> (54 % 32)) &  0x1f)
            VING_MIRR   =  ((readData[1] >> (59 % 32)) &  0x1f)


        
            def pfrmt(x): return '{0:05b}'.format(x)
            
            entry = [str(VLANID), pfrmt(VMEMB_PORT), pfrmt(VLAN_BC), pfrmt(TAG_PORT), pfrmt(VING_MIRR), pfrmt(VEGR_MIRR)]
            
            table.add_row(entry)
                    
    print()        
    print('VLAN Lookup Table Read Complete')

    print(table.get_string())
   
except Exception as e: # catch all exceptions
    print(e)

