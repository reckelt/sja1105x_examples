# sample to read status bits of MAC-Configuration Table
from sja1105_platform import PlatformHelper, SJA1105Platform

MAC_CONFIG_CONTROL = 0x53
MAC_CONFIG_DATA_START = 0x4B
MAC_CONFIG_DATA_COUNT = 8
MAC_CONFIG_DATA_OFFSET = 13 # first 13bits are reserved, ENTRY[18:0] = 31:13

# Table 22
INGMIRRDEI = 13
INGMIRRPCP = 14
INGRESS =31
EGRESS = 32


def read_mac_config_control(platform):
    regValue = platform.spi_read(MAC_CONFIG_CONTROL)

    res = {'PORTIDX': regValue & 0x7, 'RDWRSET': (regValue >> 29) & 0x1, 'ERRORS': (regValue >> 30) & 0x1}

    return res

def write_mac_config_control(platform, portidx, write=False):
    if write:
        regValue = (portidx & 0x7) | (1 << 29) |(1<<31)
    else:
        regValue = (portidx & 0x7) | (1 << 31)

    platform.spi_write(MAC_CONFIG_CONTROL, regValue)
    regValue = platform.spi_read(MAC_CONFIG_CONTROL)
    err = (regValue >> 30) & 0x1
    if err == 1:
        return False

    return True

def read_mac_config_data(platform):
    data=[]
    for reg in range(MAC_CONFIG_DATA_START, (MAC_CONFIG_DATA_START+MAC_CONFIG_DATA_COUNT)):
        regValue = platform.spi_read(reg)
        data.append(regValue)
        #print('reg: {0:02X}: {1:08X}'.format(reg, regValue))

    

    return data

def write_mac_config_data(platform, data):
    """write data[8] as list of integers"""

    for j in range(0, 8):
        reg = MAC_CONFIG_DATA_START+j
        platform.spi_write(reg, data[j])
        #print('reg: {0:02X}: {1:08X}'.format(reg, data[j]))

    return res

def decode_mac_config_data(data):
    """data is block of 8 32-bit integers"""
    ingress = (data[0] >> 31) & 0x1     # register 0x4C.Bit 12
    egress = (data[1] >> 0) & 0x1
    res = {'INGRESS': ingress, 'EGRESS': egress}

    return res

# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()

print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)

deviceId = platform.spi_read(0)

print('DeviceID: {0:08X}'.format(deviceId))


print('MAC-ConfigTable: {0}'.format(read_mac_config_control(platform)))

write_mac_config_control(platform, 0)   # select port #0


res = read_mac_config_data(platform)

print('MAC-DATA Port #0: {0}'.format(res))

print('Decode Port:#0 {0}'.format(decode_mac_config_data(res)))



write_mac_config_control(platform, 1)   # select port #1


res = read_mac_config_data(platform)

print('MAC-DATA Port #1: {0}'.format(res))



# now enable INGRESS/EGRESS on Port0

write_mac_config_control(platform, 0)   # select port #0

res = read_mac_config_data(platform)

res[0] = res[0] | (1<<31)   # Bit 31 (INGRESS)
res[1] = res[1] | 1         # Bit 32 (EGRESS)

write_mac_config_data(platform, res)

write_mac_config_control(platform, 0, True)

# and read back to verify result

write_mac_config_control(platform, 0)   # select port #0

res = read_mac_config_data(platform)

print('MAC-DATA Port #0: {0}'.format(res))

print('Decode Port:#0 {0}'.format(decode_mac_config_data(res)))
