"""
* Copyright (C) NXP Semiconductors 2017. All rights reserved.
* Disclaimer
* 1. The NXP Software/Source Code is provided to Licensee "AS IS" without any
* warranties of any kind. NXP makes no warranties to Licensee and shall not
* indemnify Licensee or hold it harmless or any reason related to the NXP
* Software/Source Code or otherwise be liable to the NXP customer. The NXP
* customer acknowledges and agrees that the NXP Software/Source Code is
* provided AS-IS and accepts all risks of utilizing the NXP Software under the
* conditions set forth according to this disclaimer.
*
* 2. NXP EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS. NXP
* SHALL HAVE NO LIABILITY TO THE NXP CUSTOMER, OR ITS SUBSIDIARIES, AFFILIATES,
* OR ANY OTHER THIRD PARTY FOR ANY DAMAGES, INCLUDING WITHOUT LIMITATION,
* DAMAGES RESULTING OR ALLEGED TO HAVE RESULTED FROM ANY DEFECT, ERROR OR
* OMISSION IN THE NXP SOFTWARE/SOURCE CODE, THIRD PARTY APPLICATION SOFTWARE
* AND/OR DOCUMENTATION, OR AS A RESULT OF ANY INFRINGEMENT OF ANY INTELLECTUAL
* PROPERTY RIGHT OF ANY THIRD PARTY. IN NO EVENT SHALL NXP
* BE LIABLE FOR ANY INCIDENTAL, INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR
* CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS) SUFFERED BY NXP CUSTOMER OR
* ITS SUBSIDIARIES, AFFILIATES, OR ANY OTHER THIRD PARTY ARISING OUT OF OR
* RELATED TO THE NXP SOFTWARE/SOURCE CODE EVEN IF NXP HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
"""

import sys
sys.path.append('..\modules')

import uart as uart_platform
from modules import prettytable

try:
    platform = uart_platform.UartPlatform("auto")    # the integer between the brackets specify which board to use. This value corresponds to encoder X10 on the board
except Exception as e:
    print e
    exit(0)


##Logging
LOG_FILE = 0  ##1 -> Enable Logging; 0 -> Disable Logging


##Configurable values
DESTPORTS           = 0x13            ## (_ _ _ P4 P3 P2 P1 P0 ) Destination Ports.
INDEX               = 5               ## TCAM index (randomly chosen)
MACADDR             = 0x112233445566  ## MAC address of the management frame.
MACADDR_MASK        = 0xffffffffffff  ## 0xffffffffffff = all MACADDR bits valid
VLANID              = 444             ## VLANID                     
VLANID_MASK         = 0xfff           ## 0xfff = all VLANID bits valid
IOTAG               = 0               ## 0 -> C-Tag; 1 -> S-Tag
IOTAG_MASK          = 1               ## 0 = don't care;
ENFPORT             = 1               ## Enforce MAC to appear only on designated ports

##Values to remain unchanged
VALID               = 1               ##Triggers dynamic change
RDWRSET             = 1               ##0 -> Read; 1 -> Write
VALIDENT            = 1               ##1 -> written entry is valid; 0 -> dispose written entry
HOSTCMD             = 3               ##2 -> read; 3 -> write
MGMTROUTE           = 0               ##0 -> L2 entry 1 -> Management entry
L2_ADDRESS          = 0x24            ##Start address of the L2 address lookup table
L2_CONTROL_ADDRESS  = 0x29            ##L2 address lookup table control register
TABLE_SIZE          = 1024            ##Table size
ADDRESS_RANGE       = 5               ##Number of addresses to be read
NUM_PORTS           = 5               ##Number of ports
MAC_LENGTH          = 6               ##Length of MAC address



def mac_to_str(mac):
    return ':'.join(['%02X' % ((MACADDR >> (8 * (6-1-k))) & 0xFF) for k in range(6)])

l2Entry = []
##L2 Entry
tmp  = 0
tmp |= ((INDEX & 0x03ff)               << ( 6 % 32))
tmp |= ((ENFPORT & 1)                  << (16 % 32))
tmp |= ((DESTPORTS & 0x1f)             << (17 % 32))
tmp |= ((MACADDR & 0x3ff)              << (22 % 32))
l2Entry.append(tmp) # 0
tmp  = 0
tmp |= ((MACADDR >> 10) & 0xffffffff)
l2Entry.append(tmp) # 1
tmp  = 0
tmp |= (((MACADDR >> 42) & 0x3f)       << (64 % 32))
tmp |= ((VLANID & 0xfff)               << (70 % 32))
tmp |= ((IOTAG & 1)                    << (82 % 32))
tmp |= ((MACADDR_MASK & ((1<<13)-1))   << (83 % 32))
l2Entry.append(tmp) # 2
tmp  = 0
tmp |= ((MACADDR_MASK >> 14) & 0xffffffff)
l2Entry.append(tmp) # 3
tmp  = 0
tmp |= ((MACADDR_MASK >> 45) & 0x7     <<(128 % 32))
tmp |= ((VLANID_MASK & 0xfff)          <<(131 % 32))
tmp |= ((IOTAG_MASK & 1)               <<(143 % 32))
l2Entry.append(tmp) # 4

##Control register
controlL2Entry = ((VALID << 31) + (RDWRSET << 30) + (VALIDENT << 27) + (HOSTCMD << 23))
   
try:
    ##Writing to the lookup table
    for i in range (len(l2Entry)):
        platform.spi_write(L2_ADDRESS + i, l2Entry[i])
            
    ##Write Control
    platform.spi_write(L2_CONTROL_ADDRESS, controlL2Entry)

    table = prettytable.PrettyTable(("INDEX", "DESTPORTS", "MACADDR", "VLANID", "ENFPORT", "IOTAG"))    
    entry = (str(INDEX), '{0:05b}'.format(DESTPORTS), mac_to_str(MACADDR), str(VLANID), str(ENFPORT), str(IOTAG))
    table.add_row(entry)
    print table.get_string()
    
    print "L2 Address Lookup Table Entry Written"
   
except Exception as e: # catch all exceptions
    print e
