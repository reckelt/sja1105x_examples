from sja1105_platform import PlatformHelper, SJA1105Platform


# SJA1105x Helper to detect EVB
hlp = PlatformHelper()
hlp.detect()



print('NumPlatforms found: {0} {1}'.format(hlp.NumPlatformsFound, hlp.platformPortMap))
print()

if hlp.NumPlatformsFound != 1:
    print('Connect exactly 1 SJA1105 EVB!')
    sys.exit(-1)

port = hlp.open_by_index(0)

platform = SJA1105Platform(port)

platform.reset()

hlp.close(port)

